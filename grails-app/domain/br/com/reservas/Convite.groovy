package br.com.reservas

import java.text.SimpleDateFormat;
import java.util.Date;

class Convite {

	Usuario usuario
	Date dataConvite
	Date dataAceite
	String email
	Boolean usuarioSolicitou
	Boolean aprovado
	
	static belongsTo = [apartamento:Apartamento]
	
    static constraints = {
		usuario nullable:true 
		dataConvite nullable:false 
		dataAceite nullable:true 
		email blank:false, nullable: false
		usuarioSolicitou nullable: false
		aprovado nullable: true
    }
	
	static mapping = {
		usuarioSolicitou defaultValue:false
		aprovado defaultValue:null
	}
	
	String encodeAsHTML(){
		StringBuilder sb = new StringBuilder()
		
		sb.append(usuario)
		sb.append(" (")
		sb.append(email)
		sb.append(")")
		
		sb.toString()
	}
	
	String formatarData(Date data){
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy")
		return sdf.format(data).toString()
	}
	
	String formatarDataHora(Date data){
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
		return sdf.format(data).toString()
	}
}
