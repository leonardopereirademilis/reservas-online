<%@ page import="br.com.reservas.Usuario" %>



<div class="form-group ${hasErrors(bean: usuarioInstance, field: 'nome', 'error')} required">
	<label for="nome">
		<i class="glyphicon glyphicon-user"></i> 
		<g:message code="usuario.nome.label" default="Nome" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nome" required="" value="${usuarioInstance?.nome}" class="form-control"/>
</div>

<%--<div class="form-group ${hasErrors(bean: usuarioInstance, field: 'username', 'error')} required">--%>
<%--	<label for="username">--%>
<%--		<g:message code="usuario.username.label" default="Username" />--%>
<%--		<span class="required-indicator">*</span>--%>
<%--	</label>--%>
<%--	<g:textField name="username" required="" value="${usuarioInstance?.username}"/>--%>
<%--</div>--%>

<div class="form-group ${hasErrors(bean: usuarioInstance, field: 'email', 'error')} required">
	<label for="email">
		<i class="glyphicon glyphicon-envelope"></i> 
		<g:message code="usuario.email.label" default="E-mail" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="email" required="" value="${usuarioInstance?.email}" class="form-control"/>
</div>


<div class="form-group ${hasErrors(bean: usuarioInstance, field: 'password', 'error')} required">
	<label for="password">
		<i class="glyphicon glyphicon-asterisk"></i> 
		<g:message code="usuario.password.label" default="Password" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="password" type="password" required="" value="${usuarioInstance?.password}" class="form-control"/>
</div>

<sec:ifAnyGranted roles="ROLE_ADMIN">
	<div class="form-group ${hasErrors(bean: usuarioInstance, field: 'accountExpired', 'error')} ">
		<label for="accountExpired">
			<i class="glyphicon glyphicon-ok-sign"></i> 
			<g:message code="usuario.accountExpired.label" default="Account Expired" />
			
		</label>
		<div>
			<g:checkBox name="accountExpired" value="${usuarioInstance?.accountExpired}" />
		</div>	
	</div>
	
	<div class="form-group ${hasErrors(bean: usuarioInstance, field: 'accountLocked', 'error')} ">
		<label for="accountLocked">
			<i class="glyphicon glyphicon-lock"></i> 
			<g:message code="usuario.accountLocked.label" default="Account Locked" />
			
		</label>
		<div>
		<g:checkBox name="accountLocked" value="${usuarioInstance?.accountLocked}" />
		</div>
	</div>
	
	<div class="form-group ${hasErrors(bean: usuarioInstance, field: 'enabled', 'error')} ">
		<label for="enabled">
			<i class="glyphicon glyphicon-ok-sign"></i> 
			<g:message code="usuario.enabled.label" default="Enabled" />
			
		</label>
		<div>
		<g:checkBox name="enabled" value="${usuarioInstance?.enabled}" />
		</div>
	</div>
	
	<div class="form-group ${hasErrors(bean: usuarioInstance, field: 'passwordExpired', 'error')} ">
		<label for="passwordExpired">
			<i class="glyphicon glyphicon-ok-sign"></i> 
			<g:message code="usuario.passwordExpired.label" default="Password Expired" />
			
		</label>
		<div>
		<g:checkBox name="passwordExpired" value="${usuarioInstance?.passwordExpired}" />
		</div>
	</div>
	
	<div class="form-group ${hasErrors(bean: usuarioInstance, field: 'planos', 'error')} ">
		<label for="planos">
			<i class="glyphicon glyphicon-list"></i> 
			<g:message code="usuario.planos.label" default="Planos" />
			
		</label>
		<g:select name="planos" from="${br.com.reservas.Plano.list()}" multiple="multiple" optionKey="id" size="5" value="${usuarioInstance?.planos*.id}" class="form-control many-to-many"/>
	</div>
</sec:ifAnyGranted>

<g:hiddenField name="enabled" value="true"/>
