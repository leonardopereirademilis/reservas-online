
<%@ page import="br.com.reservas.Usuario" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-usuario" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<ol class="breadcrumb">
			<li><a class="home" href="${createLink(uri: '/')}"><i class="glyphicon glyphicon-home"></i> <g:message code="default.home.label"/></a></li>
		</ol>
		
		<div id="show-usuario" class="container-fluid" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> ${flash.message}</div>
			</g:if>
			<div class="panel panel-default">
				<div class="panel-heading">
					<g:message code="default.show.label" args="[entityName]" />
				</div>
				<div class="panel-body">
					<div class="row">
						<g:if test="${usuarioInstance?.nome}">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="nome"><i class="glyphicon glyphicon-user"></i> <g:message code="usuario.nome.label" default="Nome" /></label>
									<h2><g:fieldValue bean="${usuarioInstance}" field="nome"/></h2>
								</div>
							</div>		
						</g:if>
			
						<g:if test="${usuarioInstance?.email}">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="email"><i class="glyphicon glyphicon-envelope"></i> <g:message code="usuario.email.label" default="E-mail" /></label>
									<div>
										<g:fieldValue bean="${usuarioInstance}" field="email"/>
									</div>	
								</div>
							</div>		
						</g:if>
					</div>	
			
					<hr>
				
					<div class="row">
						<g:if test="${usuarioInstance?.accountExpired}">
							<div class="col-sm-3">
								<div class="form-group">
									<label for="accountExpired"><i class="glyphicon glyphicon-time"></i> <g:message code="usuario.accountExpired.label" default="Conta expirou" /></label>
									<div>
										<g:formatBoolean boolean="${usuarioInstance?.accountExpired}" />
									</div>	
								</div>
							</div>						
						</g:if>
					
						<g:if test="${usuarioInstance?.accountLocked}">
							<div class="col-sm-3">
								<div class="form-group">
									<label for="accountLocked"><i class="glyphicon glyphicon-lock"></i> <g:message code="usuario.accountLocked.label" default="Conta bloqueada" /></label>
									<div>
										<g:formatBoolean boolean="${usuarioInstance?.accountLocked}" />
									</div>	
								</div>
							</div>								
						</g:if>
					
						<g:if test="${usuarioInstance?.enabled}">
							<div class="col-sm-3">
								<div class="form-group">
									<label for="enabled"><i class="glyphicon glyphicon-ok-sign"></i> <g:message code="usuario.enabled.label" default="Ativo" /></label>
									<div>
										<g:formatBoolean boolean="${usuarioInstance?.enabled}" />
									</div>	
								</div>
							</div>	
						</g:if>
					
						<g:if test="${usuarioInstance?.passwordExpired}">
							<div class="col-sm-3">
								<div class="form-group">
									<label for="passwordExpired"><i class="glyphicon glyphicon-time"></i> <g:message code="usuario.passwordExpired.label" default="Password Expirou" /></label>
									<div>
										<g:formatBoolean boolean="${usuarioInstance?.passwordExpired}" />
									</div>	
								</div>
							</div>		
						</g:if>
					</div>
								
					<g:if test="${ehUsuarioLogado}">
						<g:if test="${usuarioInstance?.planos}">
							<label for="planos"><i class="glyphicon glyphicon-list"></i> <g:message code="usuario.planos.label" default="Planos" /></label>
							<ul class="list-group">
								<g:each in="${usuarioInstance.planos}" var="p">
									<g:if test="${p?.id}">
										<li class="list-group-item">
											<g:link controller="plano" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link>
										<li class="list-group-item">	
									</g:if>	
								</g:each>
							</ul>
						</g:if>
					</g:if>	
			
				</div>
				<g:if test="${ehUsuarioLogado}">
					<div class="panel-footer">
						<div class="row">
							<div class="col-sm-12">
								<div class="pull-right">
									<g:form action="delete">
										<g:hiddenField name="id" value="${usuarioInstance?.id}" />
										<g:link class="btn btn-primary" action="edit" id="${usuarioInstance?.id}"><i class="glyphicon glyphicon-pencil"></i> <g:message code="default.button.edit.label" default="Edit" /></g:link>
										<button type="submit" class="btn btn-primary" id="delete" name="delete" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Tem certeza?')}');">
						                	<i class="glyphicon glyphicon-remove"></i> ${message(code: 'default.button.delete.label', default: 'Delete')}
						                </button>
						            </g:form>
						        </div>        
							</div>
						</div>
					</div>					
				</g:if>
			</div>	
		</div>
	</body>
</html>
