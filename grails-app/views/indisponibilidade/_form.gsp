<%@ page import="br.com.reservas.Indisponibilidade" %>

<div class="row">
	<div class="col-sm-4">
		<div class="form-group ${hasErrors(bean: indisponibilidadeInstance, field: 'recurso.condominio', 'error')} required">
			<label for="condominio">
				<i class="glyphicon glyphicon-home"></i> <g:message code="indisponibilidade.condominio.label" default="Condomínio" />		
			</label>
			<div>
				<h2><g:link controller="condominio" action="show" id="${indisponibilidadeInstance?.recurso?.condominio?.id}">${indisponibilidadeInstance?.recurso?.condominio?.encodeAsHTML()}</g:link></h2>
			</div>	
		</div>
	</div>
	
	<div class="col-sm-4">	
		<div class="form-group ${hasErrors(bean: indisponibilidadeInstance, field: 'recurso', 'error')} required">
			<label for="recurso">
				<i class="glyphicon glyphicon-list"></i> <g:message code="indisponibilidade.recurso.label" default="Recurso" />
			</label>
			<div>
			<g:link controller="recurso" action="show" id="${indisponibilidadeInstance?.recurso?.id}">${indisponibilidadeInstance?.recurso?.encodeAsHTML()}</g:link>
			</div>
		</div>
	</div>
</div>		

<g:hiddenField id="recurso" name="recurso.id" value="${indisponibilidadeInstance?.recurso?.id}" />

<hr>

<div class="row">
	<div class="col-sm-4">
		<div class="form-group ${hasErrors(bean: indisponibilidadeInstance, field: 'dataInicio', 'error')} required">
			<label for="dataInicio">
				<i class="glyphicon glyphicon-time"></i> <g:message code="indisponibilidade.dataInicio.label" default="Data de início" />
				<span class="required-indicator">*</span>
			</label>
			<div class="datepicker-day">
				<g:datePicker name="dataInicio" precision="day"  value="${indisponibilidadeInstance?.dataInicio}"  />
			</div>	
		</div>
	</div>
	
	<div class="col-sm-4">	
		<div class="form-group ${hasErrors(bean: indisponibilidadeInstance, field: 'dataFim', 'error')} required">
			<label for="dataFim">
				<i class="glyphicon glyphicon-time"></i> <g:message code="indisponibilidade.dataFim.label" default="Data de fim" />
				<span class="required-indicator">*</span>
			</label>
			<div class="datepicker-day">
				<g:datePicker name="dataFim" precision="day"  value="${indisponibilidadeInstance?.dataFim}"  />
			</div>	
		</div>
	</div>

	<div class="col-sm-4">	
		<div class="form-group ${hasErrors(bean: indisponibilidadeInstance, field: 'ativo', 'error')} ">
			<label for="ativo">
				<i class="glyphicon glyphicon-ok-sign"></i> <g:message code="indisponibilidade.ativo.label" default="Ativo" />
				
			</label>
			<div>
				<g:checkBox name="ativo" value="1" />
			</div>	
		</div>
	</div>
</div>		

<div class="row">
	<div class="col-sm-12">
		<div class="form-group ${hasErrors(bean: indisponibilidadeInstance, field: 'motivo', 'error')} ">
			<label for="motivo">
				<i class="glyphicon glyphicon-info-sign"></i> <g:message code="indisponibilidade.motivo.label" default="Motivo" />
				
			</label>
			<g:textField class="form-control" name="motivo" value="${indisponibilidadeInstance?.motivo}"/>
		</div>
	</div>
</div>		