<%@ page import="br.com.reservas.Endereco" %>


<div class="row">
	<div class="col-sm-4">
		<div class="form-group ${hasErrors(bean: enderecoInstance, field: 'logradouro', 'error')} required">
			<label for="logradouro">
				<i class="glyphicon glyphicon-map-marker"></i> <g:message code="endereco.logradouro.label" default="Logradouro" />
				<span class="required-indicator">*</span>
			</label>
			<g:textField class="form-control" name="logradouro" value="${enderecoInstance?.logradouro}" required=""/>
		</div>
	</div>	

	<div class="col-sm-4">
		<div class="form-group ${hasErrors(bean: enderecoInstance, field: 'complemento', 'error')} ">
			<label for="complemento">
				<i class="glyphicon glyphicon-map-marker"></i> <g:message code="endereco.complemento.label" default="Complemento" />
				
			</label>
			<g:textField class="form-control" name="complemento" value="${enderecoInstance?.complemento}"/>
		</div>
	</div>

	<div class="col-sm-4">		
		<div class="form-group ${hasErrors(bean: enderecoInstance, field: 'numero', 'error')} required">
			<label for="numero">
				<i class="glyphicon glyphicon-map-marker"></i> <g:message code="endereco.numero.label" default="Número" />
				<span class="required-indicator">*</span>
			</label>
			<g:field class="form-control" name="numero" type="number" value="${enderecoInstance?.numero}" required=""/>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-4">		
		<div class="form-group ${hasErrors(bean: enderecoInstance, field: 'bairro', 'error')} required">
			<label for="bairro">
				<i class="glyphicon glyphicon-map-marker"></i> <g:message code="endereco.bairro.label" default="Bairro" />
				<span class="required-indicator">*</span>
			</label>
			<g:textField class="form-control" name="bairro" value="${enderecoInstance?.bairro}" required=""/>
		</div>
	</div>	

	<div class="col-sm-4">
		<div class="form-group ${hasErrors(bean: enderecoInstance, field: 'cep', 'error')} ">
			<label for="cep">
				<i class="glyphicon glyphicon-map-marker"></i> <g:message code="endereco.cep.label" default="CEP" />
				
			</label>
			<g:textField class="form-control" name="cep" value="${enderecoInstance?.cep}"/>
		</div>
	</div>

	<div class="col-sm-4">		
		<div class="form-group ${hasErrors(bean: enderecoInstance, field: 'estado', 'error')} required">
			<label for="estado">
				<i class="glyphicon glyphicon-map-marker"></i> <g:message code="endereco.estado.label" default="Estado" />
				<span class="required-indicator">*</span>
			</label>
			<g:select id="estado" name="estado.id" from="${br.com.reservas.Estado.list()}" optionKey="id" required="" value="${enderecoInstance?.cidade?.estado?.id}" class="form-control many-to-one" noSelection="['':'Selecione um Estado...']" onchange="${remoteFunction(controller: 'cidade', action: 'buscaCidades', params: '\'estado=\' + this.value', update:'cidadeSelect')}" />
				
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-4">		
		<div class="form-group ${hasErrors(bean: enderecoInstance, field: 'cidade', 'error')} required">
			<label for="cidade">
				<i class="glyphicon glyphicon-map-marker"></i> <g:message code="endereco.cidade.label" default="Cidade" />
				<span class="required-indicator">*</span>
			</label>
			<div id="cidadeSelect" style="display: inline;">
				<g:render template="/cidade/cidade" model="[cidades: cidades, cidade:enderecoInstance?.cidade?.id]" ></g:render>
			</div>
		</div>
	</div>
</div>		

