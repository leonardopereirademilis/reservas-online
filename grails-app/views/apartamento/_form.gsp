<%@ page import="br.com.reservas.Apartamento" %>

<div class="row">
	<div class="col-sm-6">

		<g:if test="${apartamentoInstance?.condominio}">
			<div class="form-group">
				<label for="condominio">
					<i class="glyphicon glyphicon-home"></i> <g:message code="apartamento.condominio.label" default="Nome do condomínio" />			
				</label>
				
				<h2>
					<g:link controller="condominio" action="show" id="${apartamentoInstance?.condominio?.id}">${apartamentoInstance?.condominio?.encodeAsHTML()}</g:link>
				</h2>	
							
			</div>
		</g:if>
	</div>
</div>

<hr>
	
<div class="row">	
	<div class="col-sm-6">

		<div class="form-group ${hasErrors(bean: apartamentoInstance, field: 'numero', 'error')} required">
			<label for="numero">
				<i class="glyphicon glyphicon-list"></i> <g:message code="apartamento.numero.label" default="Número do apartamento" />
				<span class="required-indicator">*</span>
			</label>
			<g:field class="form-control" name="numero" type="text" value="${apartamentoInstance.numero}" required=""/>	
			<br>
			<div class="alert alert-warning">
				<g:message code="apartamento.numero.info.label" default="" />
			</div>	
			
		</div>
	</div>	
	
	<div class="col-sm-6">

		<div class="form-group ${hasErrors(bean: apartamentoInstance, field: 'bloco', 'error')} ">
			<label for="bloco">
				<i class="glyphicon glyphicon-th-large"></i> <g:message code="apartamento.bloco.label" default="Bloco" />
				
			</label>
			<g:textField class="form-control" name="bloco" value="${apartamentoInstance?.bloco}"/>
			<br>
			<div class="alert alert-warning">
				<g:message code="apartamento.bloco.info.label" default="" />
			</div>	
		</div>
	</div>
</div>		

<g:hiddenField id="condominio" name="condominio.id" required="" value="${apartamentoInstance?.condominio?.id}" />

<hr>

<div class="row">	
	<div class="col-sm-12">
	
		<div class="form-group ${hasErrors(bean: apartamentoInstance, field: 'convites', 'error')} ">
			<label for="convites">
				<i class="glyphicon glyphicon-envelope"></i> <g:message code="apartamento.convites.label" default="Convites" />
				
			</label>
			
			<ul class="list-group">
				<g:each in="${apartamentoInstance?.convites?}" var="c">
				    <li class="list-group-item">
				    	<g:link controller="convite" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link>		    		
				    </li>
				</g:each>
			</ul>		
				
			<div class="pull-right">
				<g:if test="${apartamentoInstance?.id}">
					<g:link class="btn btn-default" controller="convite" action="create" params="['apartamento.id': apartamentoInstance?.id]"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'convite.label', default: 'Convite')])}</g:link>	
				</g:if>
				<g:else>
					<div class="btn btn-default disabled"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'convite.label', default: 'Convite')])}</div>				
				</g:else>			
			</div>
		
		</div>
	</div>
</div>		

