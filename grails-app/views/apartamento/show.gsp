
<%@ page import="br.com.reservas.Apartamento" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'apartamento.label', default: 'Apartamento')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-apartamento" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<ol class="breadcrumb">
			<li><a class="home" href="${createLink(uri: '/')}"><i class="glyphicon glyphicon-home"></i> <g:message code="default.home.label"/></a></li>
			<li><g:link class="list" action="list"><i class="glyphicon glyphicon-list"></i> <g:message code="default.list.label" args="[entityName]" /></g:link></li>
			<li><g:link class="create" action="create"><i class="glyphicon glyphicon-plus"></i> <g:message code="default.new.label" args="[entityName]" /></g:link></li>
		</ol>
		<div id="show-apartamento" class="container-fluid" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> ${flash.message}</div>
			</g:if>
			<div class="panel panel-default">
				<div class="panel-heading">
					<g:message code="default.show.label" args="[entityName]" />
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
			
								<g:if test="${apartamentoInstance?.condominio}">
									<label for="condominio"><i class="glyphicon glyphicon-home"></i> <g:message code="apartamento.condominio.label" default="Nome do condomínio" /></label>
									
									<h2><g:link controller="condominio" action="show" id="${apartamentoInstance?.condominio?.id}">${apartamentoInstance?.condominio?.encodeAsHTML()}</g:link></h2>
								
								</g:if>
							</div>
						</div>
					</div>
								
					<g:if test="${apartamentoInstance?.numero || apartamentoInstance?.bloco}">
						<hr>
						<div class="row">
							<g:if test="${apartamentoInstance?.numero}">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="numero"><i class="glyphicon glyphicon-list"></i> <g:message code="apartamento.numero.label" default="Número do apartamento" /></label>
							
										<div>
											<g:fieldValue bean="${apartamentoInstance}" field="numero"/>
										</div>	
									</div>
								</div>							
							</g:if>
							
							<g:if test="${apartamentoInstance?.bloco}">
								<div class="col-sm-6">
									<label for="bloco"><i class="glyphicon glyphicon-th-large"></i> <g:message code="apartamento.bloco.label" default="Bloco" /></label>
					
									<div>
										<g:fieldValue bean="${apartamentoInstance}" field="bloco"/>
									</div>	
								</div>
							</g:if>
						</div>
						
						
					</g:if>		
									
					<g:if test="${apartamentoInstance?.convites}">
						<hr>
						<div class="row">
							<g:if test="${apartamentoInstance?.numero}">
								<div class="col-sm-12">
									<label for="convites"><i class="glyphicon glyphicon-envelope"></i> <g:message code="apartamento.convites.label" default="Convites" /></label>
									
									<ul class="list-group">
										<g:each in="${apartamentoInstance.convites}" var="c">
											<li class="list-group-item"><g:link controller="convite" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
										</g:each>
									</ul>
								</div>
							</g:if>
						</div>			
					</g:if>
					
					<g:if test="${ehAdministrador}">
						<hr>
						<div class="pull-right">
							<g:link class="btn btn-default" controller="convite" action="create" params="['apartamento.id': apartamentoInstance?.id]"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'convite.label', default: 'Convite')])}</g:link>				
						</div>
					</g:if>	
				</div>
				
				<div class="panel-footer">
					<div class="row">
						<div class="col-sm-12">
							<div class="pull-right">
								<g:form action="delete">
									<g:hiddenField name="id" value="${apartamentoInstance?.id}" />
									<g:link class="btn btn-primary" action="edit" id="${apartamentoInstance?.id}"><i class="glyphicon glyphicon-pencil"></i> <g:message code="default.button.edit.label" default="Edit" /></g:link>
									<button type="submit" class="btn btn-primary" id="delete" name="delete" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Tem certeza?')}');">
						            	<i class="glyphicon glyphicon-remove"></i> ${message(code: 'default.button.delete.label', default: 'Delete')}
						            </button>
						        </g:form>
							</div>        
						</div>
					</div>
				</div>	
			</div>
		</div>
	</body>
</html>
