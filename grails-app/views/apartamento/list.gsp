
<%@page import="java.lang.Boolean"%>
<%@ page import="br.com.reservas.Apartamento" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'apartamento.label', default: 'Apartamento')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-apartamento" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<ol class="breadcrumb" >
			<li><a class="home" href="${createLink(uri: '/')}"><i class="glyphicon glyphicon-home"></i> <g:message code="default.home.label"/></a></li>
			<li><g:link class="create" action="create"><i class="glyphicon glyphicon-plus"></i> <g:message code="default.new.label" args="[entityName]" /></g:link></li>
		</ol>
		
		<div id="list-apartamento" class="container-fluid" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> ${flash.message}</div>
			</g:if>
			
			<div class="panel panel-default">
				<div class="panel-heading">
					<g:message code="default.list.label" args="[entityName]" />
				</div>
				
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-12">
							<table class="table table-bordered table-striped">
								<thead>
									<tr>
									
				<%--						<g:sortableColumn property="numero" title="${message(code: 'apartamento.numero.label', default: '<i class="glyphicon glyphicon-list"></i> Número')}" />--%>
										<th><i class="glyphicon glyphicon-list"></i> <g:message code="apartamento.numero.label" default="Número" /></th>
									
				<%--						<g:sortableColumn property="bloco" title="${message(code: 'apartamento.bloco.label', default: '<i class="glyphicon glyphicon-th-large"></i> Bloco')}" />--%>
										<th><i class="glyphicon glyphicon-th-large"></i> <g:message code="apartamento.bloco.label" default="Bloco" /></th>
									
										<th><i class="glyphicon glyphicon-home"></i> <g:message code="apartamento.condominio.label" default="Condomínio" /></th>
										
										<th><i class="glyphicon glyphicon-user"></i> <g:message code="apartamento.usuario.label" default="Usuário" /></th>
									
									</tr>
								</thead>
								<tbody>
								<g:each in="${apartamentoInstanceList}" status="i" var="apartamentoInstance">
									<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
									
										<td><g:link action="show" id="${apartamentoInstance.id}">${fieldValue(bean: apartamentoInstance, field: "numero")}</g:link></td>
									
										<td>${fieldValue(bean: apartamentoInstance, field: "bloco")}</td>
									
										<td>${fieldValue(bean: apartamentoInstance, field: "condominio")}</td>
										
										<td>
											<g:each in="${apartamentoInstance.convites}" status="k" var="conviteInstance">
												<g:if test="${conviteInstance.usuario && conviteInstance.aprovado}">
													<g:link controller="usuario" action="show" id="${conviteInstance.usuario.id}">
														${conviteInstance.usuario}
													</g:link>									
												</g:if>
												<g:elseif test="${conviteInstance.aprovado == null || conviteInstance.aprovado}">
													<g:link controller="convite" action="show" id="${conviteInstance.id}">
														${conviteInstance.email}
													</g:link>
													
													<span class="pendente"><g:message code="apartamento.pendente.label" default="(Pendente)" /></span>
												</g:elseif>
												
												<br>
												 
											</g:each>
										</td>
									
									</tr>
								</g:each>
								</tbody>
							</table>
							
							<div class="row text-center">
								<div class="pagination">
									<g:paginate total="${apartamentoInstanceTotal}" params="['condominio':params.condominio]" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>					
		</div>
	</body>
</html>
