<%@ page import="br.com.reservas.Plano" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'plano.label', default: 'Plano')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#create-plano" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<ol class="breadcrumb">
			<li><a class="home" href="${createLink(uri: '/')}"><i class="glyphicon glyphicon-home"></i> <g:message code="default.home.label"/></a></li>
			<sec:ifAnyGranted roles="ROLE_ADMIN">
				<li><g:link class="list" action="list"><i class="glyphicon glyphicon-list"></i> <g:message code="default.todos.planos.label" args="[entityName]" default="Todos os Planos"/></g:link></li>
			</sec:ifAnyGranted>	
			<li><g:link class="list" action="list"><i class="glyphicon glyphicon-list"></i> <g:message code="default.meus.planos.label" args="[entityName]" default="Meus Planos" /></g:link></li>
		</ol>
		<div id="create-plano" class="container-fluid" role="main">
			<h1><g:message code="plano.ADICIONAR.label" default="Adicionar um novo Plano"/></h1>
			<g:if test="${flash.message}">
			<div class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> ${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${planoInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${planoInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
												
			<g:form action="save" >
				<div class="panel panel-default">
				    <div class="panel-heading"><g:message code="default.create.label" args="[entityName]" /></div>
				    
				    <div class="panel-body">
				    
						<g:render template="form"/>
						
					</div>	
					<div class="panel-footer">
						<div class="row">
							<div class="col-sm-12">
								<div class="pull-right">
									<button type="submit" class="btn btn-primary" id="create" name="create">
						               	<i class="glyphicon glyphicon-floppy-disk"></i> ${message(code: 'default.button.create.label', default: 'Create')}
						            </button>
								</div>
							</div>
						</div>
					</div>	
				</div>
			</g:form>
			
			<!-- INICIO FORMULARIO BOTAO PAGSEGURO -->
			<form action="https://pagseguro.uol.com.br/v2/pre-approvals/request.html" method="post">
			<!-- NÃO EDITE OS COMANDOS DAS LINHAS ABAIXO -->
			<input type="hidden" name="code" value="A2BA489E63636FA9940BAFAB383C888A" />
			<input type="hidden" name="iot" value="button" />
			<input type="image" src="https://stc.pagseguro.uol.com.br/public/img/botoes/assinaturas/209x48-contratar-cinza-assina.gif" name="submit" alt="Pague com PagSeguro - é rápido, grátis e seguro!" />
			</form>
			<!-- FINAL FORMULARIO BOTAO PAGSEGURO -->
		</div>
	</body>
</html>
