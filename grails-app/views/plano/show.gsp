
<%@ page import="br.com.reservas.Plano" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'plano.label', default: 'Plano')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-plano" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<ol class="breadcrumb">
			<li><a class="home" href="${createLink(uri: '/')}"><i class="glyphicon glyphicon-home"></i> <g:message code="default.home.label"/></a></li>
			<li><g:link class="list" action="list"><i class="glyphicon glyphicon-list"></i> <g:message code="default.list.label" args="[entityName]" /></g:link></li>
			<li><g:link class="create" action="create"><i class="glyphicon glyphicon-plus"></i> <g:message code="default.new.label" args="[entityName]" /></g:link></li>
		</ol>		
		<div id="show-plano" class="container-fluid" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> ${flash.message}</div>
			</g:if>
			
			<div class="panel panel-default">
				<div class="panel-heading">
					<g:message code="default.show.label" args="[entityName]" />
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-4">
							<g:if test="${planoInstance?.tipoPlano}">
								<label for="tipoPlano"><i class="glyphicon glyphicon-calendar"></i> <g:message code="plano.tipoPlano.label" default="Tipo de Plano" /></label>
								<div class="panel panel-default">
									<div class="panel-heading text-center">
										<h4><i class="glyphicon glyphicon-calendar"></i> Opção ${planoInstance?.tipoPlano.id}</h4>							
									</div>
									<div class="panel-body">
										<h2 class="text-center">
											<g:formatNumber number="${planoInstance?.tipoPlano.valor}" type="currency" currencyCode="BRL" />
										</h2>
											<div class="text-center">	
												${message(code: 'tipoPlano.valor.mes.label', default: 'por mês')}
											</div>
										<h4>
											<i class="glyphicon glyphicon-info-sign"></i> Informações do plano
										</h4>	
										<ul>
											<li>${message(code: 'tipoPlano.nuCondominios.label', default: 'Nº máximo de condomínios: ')}<strong>01</strong></li>
											<li>${message(code: 'tipoPlano.nuMaxApartamentos.label', default: 'Nº máximo de apartamentos: ')}<strong>${fieldValue(bean: planoInstance.tipoPlano, field: "nuMaxApartamentos")}</strong></li>
											<li>${message(code: 'tipoPlano.nuMaxRecursos.label', default: 'Nº máximo de recursos: ')}<strong>${fieldValue(bean: planoInstance?.tipoPlano, field: "nuMaxRecursos")}</strong></li>
										</ul>
									</div>									
								</div>	

							</g:if>
						</div>
					</div>
					
					<hr>
					
					<div class="row">
						<g:if test="${planoInstance?.dataInicio}">
							<div class="col-sm-4">
								<label for="dataInicio"><i class="glyphicon glyphicon-calendar"></i> <g:message code="plano.dataInicio.label" default="Data de Início" /></label>
								<div><g:formatDate format="dd/MM/yyyy" date="${planoInstance?.dataInicio}" /></div>
							</div>	
						</g:if>
						
						<g:if test="${planoInstance?.dataFim}">
							<div class="col-sm-4">
								<label for="dataFim"><i class="glyphicon glyphicon-calendar"></i> <g:message code="plano.dataFim.label" default="Data de Fim" /></label>
								<div><g:formatDate date="${planoInstance?.dataFim}" /></div>
							</div>
						</g:if>

						<g:if test="${planoInstance?.ativo}">
							<div class="col-sm-4">
								<label for="ativo"><i class="glyphicon glyphicon-ok-sign"></i> <g:message code="plano.ativo.label" default="Ativo" /></label>
								<div><g:formatBoolean boolean="${planoInstance?.ativo}" /></div>									
							</div>
						</g:if>
						
						<g:if test="${planoInstance?.usuario}">
							<div class="col-sm-4">
								<label for="usuario"><i class="glyphicon glyphicon-user"></i> <g:message code="plano.usuario.label" default="Usuário" /></label>
								<div><g:link controller="usuario" action="show" id="${planoInstance?.usuario?.id}">${planoInstance?.usuario?.encodeAsHTML()}</g:link></div>
							</div>
						</g:if>						
					</div>	
			
					<g:if test="${planoInstance?.condominio}">
						<hr>
						<div class="row">
							<div class="col-sm-12">
								<g:render template="/condominio/show" model="['condominioInstance':planoInstance?.condominio]"/>
							</div>
						</div>		
					</g:if>
				</div>
				
				<div class="panel-footer">
					<div class="row">
						<div class="col-sm-12">
							<div class="pull-right">
								<g:form action="delete">
									<fieldset class="buttons">
										<g:hiddenField name="id" value="${planoInstance?.id}" />
										<g:link class="btn btn-primary" action="edit" id="${planoInstance?.id}"><i class="glyphicon glyphicon-pencil"></i> <g:message code="default.button.edit.label" default="Edit" /></g:link>
										<button type="submit" class="btn btn-primary" id="delete" name="delete" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Tem certeza?')}');">
						                	<i class="glyphicon glyphicon-remove"></i> ${message(code: 'default.button.delete.label', default: 'Delete')}
						                </button>
									</fieldset>
								</g:form>								
							</div>
						</div>
					</div>
				</div>
			</div>	
		</div>					
	</body>
</html>
