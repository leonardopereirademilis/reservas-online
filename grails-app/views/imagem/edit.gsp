<%@ page import="br.com.reservas.Imagem" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'imagem.label', default: 'Imagem')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#edit-imagem" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<ol class="breadcrumb">
			<li><a class="home" href="${createLink(uri: '/')}"><i class="glyphicon glyphicon-home"></i> <g:message code="default.home.label"/></a></li>
			<li><g:link class="list" action="list"><i class="glyphicon glyphicon-list"></i> <g:message code="default.list.label" args="[entityName]" /></g:link></li>
			<li><g:link class="create" action="create"><i class="glyphicon glyphicon-plus"></i> <g:message code="default.new.label" args="[entityName]" /></g:link></li>
		</ol>
		<div id="edit-imagem" class="container-fluid" role="main">
			<h1><g:message code="default.edit.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> ${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${imagemInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${imagemInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form method="post"  enctype="multipart/form-data">
				<g:hiddenField name="id" value="${imagemInstance?.id}" />
				<g:hiddenField name="version" value="${imagemInstance?.version}" />
				
				<div class="panel panel-default">
				    <div class="panel-heading"><g:message code="default.edit.label" args="[entityName]" /></div>
				    
				    <div class="panel-body">
				    	<g:render template="form"/>
				    </div>
				    
				    <div class="panel-footer">
				    	<div class="row">
							<div class="col-sm-12">
								<div class="pull-right">
									<button type="submit" class="btn btn-primary" id="_action_update" name="_action_update">
						            	<i class="glyphicon glyphicon-floppy-disk"></i> ${message(code: 'default.button.update.label', default: 'Update')}
						            </button>
									
									<button type="submit" class="btn btn-primary" id="_action_delete" name="_action_delete" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Tem certeza?')}');">
						            	<i class="glyphicon glyphicon-remove"></i> ${message(code: 'default.button.delete.label', default: 'Delete')}
						            </button>							
								</div>
							</div>
						</div>
				    </div>
				</div>
			</g:form>
		</div>
	</body>
</html>
