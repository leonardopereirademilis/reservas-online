<head>
<meta name='layout' content='main' />
<title><g:message code="springSecurity.denied.title" /></title>
</head>

<body>
<div class="denied">
	<div id="denied" class="container-fluid">
		<div class="alert alert-danger">
			<i class="glyphicon glyphicon-ban-circle"></i> <g:message code="springSecurity.denied.message" />
		</div>
	</div>
</div>
</body>
