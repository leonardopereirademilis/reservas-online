<html>
<head>
	<meta name="layout" content="main"/>
	<title><g:message code="springSecurity.login.title"/></title>
	
</head>

<body>
	<div class="container">		
		<div id="login" class="container-fluid">
			<div class="panel panel-default">
				<div class="panel-heading">
					<i class="glyphicon glyphicon-log-in"></i> <g:message code="springSecurity.login.header"/>
				</div>
				
				<form role="form" action="${postUrl}" method="POST" id="loginForm" autocomplete="off">
					<div class="panel-body">
						<g:if test="${flash.message}">
							<div class="alert alert-danger">
								${flash.message}
							</div>
						</g:if>
						
						<div class="form-group">
							<label for="username">
	                        	<i class="glyphicon glyphicon-user"></i> <g:message code="springSecurity.login.username.label"/>
	                        </label>
							<input type="text" class="form-control text_" name="j_username" id="username" value="usuario@email.com.br"/>
	                    </div>    
												
						<div class="form-group">
							<label for="password">
								<i class="glyphicon glyphicon-lock"></i> <g:message code="springSecurity.login.password.label"/>
							</label>
							<input type="password" class="form-control text_" name="j_password" id="password" value="123456"/>
						</div>
						
						<div class="checkbox" id="remember_me_holder">
							<label>
								<input type="checkbox" name="${rememberMeParameter}" id="remember_me" <g:if test="${hasCookie}">checked="checked"</g:if>/> <g:message code="springSecurity.login.remember.me.label"/>
							</label>
						</div>
						
						<div class="form-group pull-right">
							<label for="registre"></label>
							<g:link class="btn btn-default" controller="usuario" action="create"><i class="glyphicon glyphicon-plus"></i> Registre-se</g:link>
						</div>
							
					</div>	
							
					<div class="panel-footer text-right">						
	                    <button type="submit" class="btn btn-primary" id="submit">
	                        <i class="glyphicon glyphicon-log-in"></i> ${message(code: "springSecurity.login.button")}
	                    </button>
	                </div>
				</form>
			</div>
		</div>
	</div>
<script type="text/javascript">
	<!--
	(function() {
		document.forms['loginForm'].elements['j_username'].focus();
	})();
	// -->
</script>
</body>
</html>
