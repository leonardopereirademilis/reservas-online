
<%@ page import="br.com.reservas.Convidado" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'convidado.label', default: 'Convidado')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-convidado" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<ol class="breadcrumb">
			<li><a class="home" href="${createLink(uri: '/')}"><i class="glyphicon glyphicon-home"></i> <g:message code="default.home.label"/></a></li>
			<li><g:link class="list" action="list"><i class="glyphicon glyphicon-list"></i> <g:message code="default.list.label" args="[entityName]" /></g:link></li>
			<li><g:link class="create" action="create"><i class="glyphicon glyphicon-plus"></i> <g:message code="default.new.label" args="[entityName]" /></g:link></li>
		</ol>
		
		<div id="show-convidado" class="container-fluid" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> ${flash.message}</div>
			</g:if>
			
			<div class="panel panel-default">
				<div class="panel-heading">
					<g:message code="default.show.label" args="[entityName]" />
				</div>
				
				<div class="panel-body">
					<div class="row">
						<g:if test="${convidadoInstance?.nome}">
							<div class="col-sm-3">
								<span id="nome-label" class="property-label"><g:message code="convidado.nome.label" default="Nome" /></span>
								<span class="property-value" aria-labelledby="nome-label"><g:fieldValue bean="${convidadoInstance}" field="nome"/></span>
							</div>		
						</g:if>
							
						<g:if test="${convidadoInstance?.cpf}">
							<div class="col-sm-3">
								<span id="cpf-label" class="property-label"><g:message code="convidado.cpf.label" default="Cpf" /></span>
								<span class="property-value" aria-labelledby="cpf-label"><g:fieldValue bean="${convidadoInstance}" field="cpf"/></span>
							</div>
						</g:if>
							
						<g:if test="${convidadoInstance?.email}">
							<div class="col-sm-3">
								<span id="email-label" class="property-label"><g:message code="convidado.email.label" default="Email" /></span>
								<span class="property-value" aria-labelledby="email-label"><g:fieldValue bean="${convidadoInstance}" field="email"/></span>
							</div>
						</g:if>
													
						<g:if test="${convidadoInstance?.telefone}">
							<div class="col-sm-3">
								<span id="telefone-label" class="property-label"><g:message code="convidado.telefone.label" default="Telefone" /></span>
								<span class="property-value" aria-labelledby="telefone-label"><g:fieldValue bean="${convidadoInstance}" field="telefone"/></span>
							</div>		
						</g:if>
					</div>
					
					
					
					<g:if test="${convidadoInstance?.reserva}">
						<hr>
						<div class="row">
							<div class="col-sm-12">
								<span id="reserva-label" class="property-label"><g:message code="convidado.reserva.label" default="Reserva" /></span>
								<span class="property-value" aria-labelledby="reserva-label"><g:link controller="reserva" action="show" id="${convidadoInstance?.reserva?.id}">${convidadoInstance?.reserva?.encodeAsHTML()}</g:link></span>
							</div>
						</div>	
					</g:if>
				</div>
				
				<div class="panel-footer">
					<div class="row">
						<div class="col-sm-12">
							<div class="pull-right">
								<g:form action="delete">
									<fieldset class="buttons">
										<g:hiddenField name="id" value="${convidadoInstance?.id}" />
										<g:link class="btn btn-primary" action="edit" id="${convidadoInstance?.id}"><i class="glyphicon glyphicon-pencil"></i> <g:message code="default.button.edit.label" default="Edit" /></g:link>
										<button type="submit" class="btn btn-primary" id="delete" name="delete" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Tem certeza?')}');">
						                	<i class="glyphicon glyphicon-remove"></i> ${message(code: 'default.button.delete.label', default: 'Delete')}
						                </button>
									</fieldset>
								</g:form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</body>
</html>
