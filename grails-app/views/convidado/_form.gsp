<%@ page import="br.com.reservas.Convidado" %>


<div class="row">
	<div class="col-sm-3">
		<div class="form-group ${hasErrors(bean: convidadoInstance, field: 'nome', 'error')} required">
			<label for="nome">
				<i class="glyphicon glyphicon-user"></i>
				<g:message code="convidado.nome.label" default="Nome" />
				<span class="required-indicator">*</span>
			</label>
			<g:textField class="form-control" name="nome" value="${convidadoInstance?.nome}" required=""/>
		</div>
	</div>
	
	<div class="col-sm-3">	
		<div class="form-group ${hasErrors(bean: convidadoInstance, field: 'cpf', 'error')} ">
			<label for="cpf">
				<i class="glyphicon glyphicon-info-sign"></i>
				<g:message code="convidado.cpf.label" default="Cpf" />
				
			</label>
			<g:textField class="form-control" name="cpf" value="${convidadoInstance?.cpf}"/>
		</div>
	</div>
	
	<div class="col-sm-3">	
		<div class="form-group ${hasErrors(bean: convidadoInstance, field: 'email', 'error')} ">
			<label for="email">
				<i class="glyphicon glyphicon-envelope"></i>
				<g:message code="convidado.email.label" default="Email" />
				
			</label>
			<g:textField class="form-control" name="email" value="${convidadoInstance?.email}"/>
		</div>
	</div>
	
	<g:hiddenField id="reserva" name="reserva.id" value="${convidadoInstance?.reserva?.id}"/>

	<div class="col-sm-3">
		<div class="form-group ${hasErrors(bean: convidadoInstance, field: 'telefone', 'error')} ">
			<label for="telefone">
				<i class="glyphicon glyphicon-earphone"></i>
				<g:message code="convidado.telefone.label" default="Telefone" />
				
			</label>
			<g:textField class="form-control" name="telefone" value="${convidadoInstance?.telefone}"/>
		</div>
	</div>
</div>		

