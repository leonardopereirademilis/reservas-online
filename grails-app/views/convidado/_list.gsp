			<h1><g:message code="default.convidado.list.label" default="Lista de Convidados" /></h1>
<%--			<g:if test="${flash.message}">--%>
<%--			<div class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> ${flash.message}</div>--%>
<%--			</g:if>--%>
			<g:if test="${convidadoInstanceList.size() > 0}">
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
						
							<th><i class="glyphicon glyphicon-user"></i> ${message(code: 'convidado.nome.label', default: 'Nome')}</th>
						
							<th><i class="glyphicon glyphicon-info"></i> ${message(code: 'convidado.cpf.label', default: 'Cpf')}</th>
						
							<th><i class="glyphicon glyphicon-envelope"></i> ${message(code: 'convidado.email.label', default: 'Email')}</th>
						
							<th><i class="glyphicon glyphicon-earphone"></i> ${message(code: 'convidado.telefone.label', default: 'Telefone')}</th>
						
						</tr>
					</thead>
					<tbody>
					<g:each in="${convidadoInstanceList}" status="i" var="convidadoInstance">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						
							<td><g:link controller="convidado" action="show" id="${convidadoInstance.id}">${fieldValue(bean: convidadoInstance, field: "nome")}</g:link></td>
						
							<td>${fieldValue(bean: convidadoInstance, field: "cpf")}</td>
						
							<td>${fieldValue(bean: convidadoInstance, field: "email")}</td>
											
							<td>${fieldValue(bean: convidadoInstance, field: "telefone")}</td>
						
						</tr>
					</g:each>
					</tbody>
				</table>
				<div class="pagination">
					<g:paginate total="${convidadoInstanceTotal}" />
				</div>
			</g:if>
			<g:else>
				<div class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> 
					<g:message code="condominio.nenhum.resultado.label" default="Nenhum convidadona lista" />
				</div>
			</g:else>	