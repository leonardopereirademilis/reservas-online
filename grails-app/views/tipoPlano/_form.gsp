<%@ page import="br.com.reservas.TipoPlano" %>


<div class="row">
	<div class="col-sm-6">
		<div class="form-group ${hasErrors(bean: tipoPlanoInstance, field: 'dataCriacao', 'error')} required">
			<label for="dataCriacao">
				<i class="glyphicon glyphicon-calendar"></i> 
				<g:message code="tipoPlano.dataCriacao.label" default="Data Criacao" />
				<span class="required-indicator">*</span>
			</label>
			<div class="datepicker-day">
				<g:datePicker name="dataCriacao" precision="day"  value="${tipoPlanoInstance?.dataCriacao}"  />
			</div>	
		</div>
	</div>
	
	<div class="col-sm-6">	
		<div class="form-group ${hasErrors(bean: tipoPlanoInstance, field: 'dataEncerramento', 'error')} ">
			<label for="dataEncerramento">
				<i class="glyphicon glyphicon-calendar"></i> 
				<g:message code="tipoPlano.dataEncerramento.label" default="Data Encerramento" />
				
			</label>
			<div class="datepicker-day">
				<g:datePicker name="dataEncerramento" precision="day"  value="${tipoPlanoInstance?.dataEncerramento}" default="none" noSelection="['': '']" />
			</div>	
		</div>
	</div>
</div>

<hr>

<div class="row">
	<div class="col-sm-3">		
		<div class="form-group ${hasErrors(bean: tipoPlanoInstance, field: 'valor', 'error')} required">
			<label for="valor">
				<i class="glyphicon glyphicon-usd"></i> 
				<g:message code="tipoPlano.valor.label" default="Valor" />
				<span class="required-indicator">*</span>
			</label>
			<g:field type="currency"  class="form-control" name="valor" value="${fieldValue(bean: tipoPlanoInstance, field: 'valor')}" required=""/>
		</div>
	</div>
	
	<div class="col-sm-3">	
		<div class="form-group ${hasErrors(bean: tipoPlanoInstance, field: 'ativo', 'error')} ">
			<label for="ativo">
				<i class="glyphicon glyphicon-ok-sign"></i> 
				<g:message code="tipoPlano.ativo.label" default="Ativo" />
				
			</label>
			<div>
				<g:checkBox name="ativo" value="${tipoPlanoInstance?.ativo}" />
			</div>	
		</div>
	</div>
	
	<div class="col-sm-3">	
		<div class="form-group ${hasErrors(bean: tipoPlanoInstance, field: 'nuMaxRecursos', 'error')} required">
			<label for="nuMaxRecursos">
				<i class="glyphicon glyphicon-info-sign"></i> 
				<g:message code="tipoPlano.nuMaxRecursos.label" default="Nu Max Recursos" />
				<span class="required-indicator">*</span>
			</label>
			<g:field class="form-control" name="nuMaxRecursos" type="number" value="${tipoPlanoInstance.nuMaxRecursos}" required=""/>
		</div>
	</div>
	
	<div class="col-sm-3">	
		<div class="form-group ${hasErrors(bean: tipoPlanoInstance, field: 'nuMaxApartamentos', 'error')} required">
			<label for="nuMaxApartamentos">
				<i class="glyphicon glyphicon-info-sign"></i> 
				<g:message code="tipoPlano.nuMaxApartamentos.label" default="Nu Max Apartamentos" />
				<span class="required-indicator">*</span>
			</label>
			<g:field class="form-control" name="nuMaxApartamentos" type="number" value="${tipoPlanoInstance.nuMaxApartamentos}" required=""/>
		</div>
	</div>
</div>

<hr>		

<div class="row">
	<div class="col-sm-12">
		<div class="form-group ${hasErrors(bean: recursoInstance, field: 'descricao', 'error')} ">
			<label for="descricao">
				<i class="glyphicon glyphicon-info-sign"></i> 
				<g:message code="recurso.descricao.label" default="Descrição" />
				
			</label>
			<g:textArea name="descricao" value="${recursoInstance?.descricao}"/>
		</div>
	</div>
</div>		
