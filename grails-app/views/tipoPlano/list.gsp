
<%@ page import="br.com.reservas.TipoPlano" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tipoPlano.label', default: 'TipoPlano')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-tipoPlano" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<ol class="breadcrumb">
			<li><a class="home" href="${createLink(uri: '/')}"><i class="glyphicon glyphicon-home"></i> <g:message code="default.home.label"/></a></li>
			<sec:ifAnyGranted roles="ROLE_ADMIN">
				<li><g:link class="create" action="create"><i class="glyphicon glyphicon-plus"></i> <g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</sec:ifAnyGranted>
		</ol>

		<div id="list-tipoPlano" class="container-fluid" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> ${flash.message}</div>
			</g:if>
		
			<div class="tipoPlano">
				<div class="row">
					<g:each in="${tipoPlanoInstanceList}" status="i" var="tipoPlanoInstance">
						<div class="col-sm-4">
							<g:if test="${tipoPlanoInstance.ativo}">
								<div class="panel panel-default">
									<div class="panel-heading text-center">
										<h4><i class="glyphicon glyphicon-calendar"></i> Opção ${i + 1}</h4>							
									</div>
									<div class="panel-body">
										<h2 class="text-center">
											<g:formatNumber number="${tipoPlanoInstance.valor}" type="currency" currencyCode="BRL" />
										</h2>
											<div class="text-center">	
												${message(code: 'tipoPlano.valor.mes.label', default: 'por mês')}
											</div>
										<h4>
											<i class="glyphicon glyphicon-info-sign"></i> Informações do plano
										</h4>	
										<ul>
											<li>${message(code: 'tipoPlano.nuCondominios.label', default: 'Nº máximo de condomínios: ')}<strong>01</strong></li>
											<li>${message(code: 'tipoPlano.nuMaxApartamentos.label', default: 'Nº máximo de apartamentos: ')}<strong>${fieldValue(bean: tipoPlanoInstance, field: "nuMaxApartamentos")}</strong></li>
											<li>${message(code: 'tipoPlano.nuMaxRecursos.label', default: 'Nº máximo de recursos: ')}<strong>${fieldValue(bean: tipoPlanoInstance, field: "nuMaxRecursos")}</strong></li>
										</ul>
									</div>
									<div class="panel-footer text-center">
										<g:form controller="plano" action="create">
											<g:hiddenField name="tipoPlanoInstanceId" value="${tipoPlanoInstance.id}"/>
											<button type="submit" class="btn btn-primary" id="adicionarPlano">
						                        <i class="glyphicon glyphicon-plus"></i> Adicionar Plano
						                    </button>
										</g:form>
									</div>
								</div>
							</g:if>
						</div>
					</g:each>
				</div>	
			</div>
		</div>			
	</body>
</html>
