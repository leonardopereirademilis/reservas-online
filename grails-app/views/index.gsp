<%@ page import="br.com.reservas.TipoPlano" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>Reservas Online</title>
	</head>
	<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
		<header class="page-head">
			<div class="jumbotron text-center">
				<i class="glyphicon glyphicon-calendar logo"></i>
		  		<h1>Reservas Online</h1>
		  		<p>Reserve seu evento de forma rápida e pratica</p>		  		
			</div>
		</header>
		<!-- Container (Para Você) -->
		<div id="voce" class="container-fluid">
			<div class="row">
				<div class="col-sm-4 text-center">
			      <i class="glyphicon glyphicon-user logo"></i>
			    </div>
    			<div class="col-sm-8">
    				<div class="row">
    					<div class="col-sm-12">
		    				<h2>Para você</h2><br>
		    				<p>Reserve online seus eventos de forma fácil e rápida. Escolha datas disponíveis. Veja fotos e descrições dos recursos. Tudo isso <strong>grátis</strong> para os usuários.</p>
		    			</div>
		    		</div><br>		
    				<div class="row">
    					<div class="pull-right">
	    					<g:link class="btn btn-primary" controller="condominio" action="list">
								<i class="glyphicon glyphicon-calendar"></i> Entre agora e começe a preparar seu evento
							</g:link>
						</div>
					</div>
    			</div>
    		</div>	
		</div>
		
		<!-- Container (Para Condomínio) -->
		<div id="condominio" class="container-fluid bg-grey">
			<div class="row">
				<div class="col-sm-4 text-center">
			      <i class="glyphicon glyphicon-calendar logo"></i>
			    </div>
    			<div class="col-sm-8">
    				<div class="row">
    					<div class="col-sm-12">
		    				<h2>Para seu condomínio</h2><br>
		    				<p>Gerencie de forma fácil e organizada as reservas dos recursos do seu condomínio. Permita que os recursos sejam reservados de forma online. Defina regras para as reservas. Gerencie os horários de disponibilidade. Disponibilize fotos e descriçoes dos recursos. Obtenha relatórios das reservas. Tudo isso através de planos mensais, de acordo com a sua necessidade.  </p>
		    			</div>	
    				</div><br>
    				<div class="row">
    					<div class="pull-right">
	    					<g:link class="btn btn-primary" controller="condominio" action="list">
								<i class="glyphicon glyphicon-calendar"></i> Entre agora e começe a preparar seu evento
							</g:link>
						</div>
					</div>	
    			</div>
    		</div>
		</div>
		
		<!-- Container (Planos Para Condomínio) -->
		<div id="planos" class="container-fluid">
			<h2>Planos para o seu condomínio</h2><br>
			<div class="row">
    			<div class="col-sm-12">
					<g:render template="/tipoPlano/list" model="['tipoPlanoInstanceList':TipoPlano.list()]"/>
				</div>
			</div>		
		</div>

		<!-- Container (Contact Section) -->
		<div id="contato" class="container-fluid bg-grey">
			<h2>Contato</h2>
			<div class="row">
				<div class="col-sm-5">
					<p>
						<span class="glyphicon glyphicon-map-marker"></span> Florianópolis-SC
					</p>
					<p>
						<span class="glyphicon glyphicon-phone"></span> +55 (48) 9934-9570
					</p>
					<p>
						<span class="glyphicon glyphicon-envelope"></span>
						lpdemilis@gmail.com
					</p>
				</div>
				<div class="col-sm-7 slideanim">
					<div class="row">
						<div class="col-sm-6 form-group">
							<input class="form-control" id="name" name="name"
								placeholder="Nome" type="text" required>
						</div>
						<div class="col-sm-6 form-group">
							<input class="form-control" id="email" name="email"
								placeholder="E-mail" type="email" required>
						</div>
					</div>
					<textarea class="form-control" id="comments" name="comments"
						placeholder="Comentário" rows="5"></textarea>
					<br>
					<div class="row">
						<div class="col-sm-12 form-group">
							<button class="btn btn-primary pull-right" type="submit"><i class="glyphicon glyphicon-envelope"></i> Enviar</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		
<%--		<a href="#page-body" class="skip"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--%>
<%--		<div id="status" role="complementary">--%>
<%--			<h1>Application Status</h1>--%>
<%--			<ul>--%>
<%--				<li>App version: <g:meta name="app.version"/></li>--%>
<%--				<li>Grails version: <g:meta name="app.grails.version"/></li>--%>
<%--				<li>Groovy version: ${GroovySystem.getVersion()}</li>--%>
<%--				<li>JVM version: ${System.getProperty('java.version')}</li>--%>
<%--				<li>Reloading active: ${grails.util.Environment.reloadingAgentEnabled}</li>--%>
<%--				<li>Controllers: ${grailsApplication.controllerClasses.size()}</li>--%>
<%--				<li>Domains: ${grailsApplication.domainClasses.size()}</li>--%>
<%--				<li>Services: ${grailsApplication.serviceClasses.size()}</li>--%>
<%--				<li>Tag Libraries: ${grailsApplication.tagLibClasses.size()}</li>--%>
<%--			</ul>--%>
<%--			<h1>Installed Plugins</h1>--%>
<%--			<ul>--%>
<%--				<g:each var="plugin" in="${applicationContext.getBean('pluginManager').allPlugins}">--%>
<%--					<li>${plugin.name} - ${plugin.version}</li>--%>
<%--				</g:each>--%>
<%--			</ul>--%>
<%--		</div>--%>
<%--		<div id="page-body" role="main">--%>
<%--			<h1>Welcome to Grails</h1>--%>
<%--			<p>Congratulations, you have successfully started your first Grails application! At the moment--%>
<%--			   this is the default page, feel free to modify it to either redirect to a controller or display whatever--%>
<%--			   content you may choose. Below is a list of controllers that are currently deployed in this application,--%>
<%--			   click on each to execute its default action:</p>--%>
<%----%>
<%--			<div id="controller-list" role="navigation">--%>
<%--				<h2>Available Controllers:</h2>--%>
<%--				<ul>--%>
<%--					<g:each var="c" in="${grailsApplication.controllerClasses.sort { it.fullName } }">--%>
<%--						<li class="controller"><g:link controller="${c.logicalPropertyName}">${c.fullName}</g:link></li>--%>
<%--					</g:each>--%>
<%--				</ul>--%>
<%--			</div>--%>
<%--		</div>--%>
	</body>
</html>
