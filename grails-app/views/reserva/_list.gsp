			<h1><g:message code="default.list.reservas.label" default="Lista de Reservas" /></h1>
			<g:if test="${flash.message}">
				<div class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> 
					${flash.message}
				</div>
			</g:if>
					
			<div class="panel panel-default">
				<div class="panel-heading">
					<g:message code="default.list.reservas.label" default="Lista de Reservas" />
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-12">
							<g:if test="${reservaInstanceTotal}">									
								<div id="reserva_div" class="table-responsive">
									<table class="table table-striped table-bordered">
										<thead>
											<tr>
											
												<g:sortableColumn property="recurso.condominio" title="${message(code: 'reserva.recurso.condominio.label', default: '<i class="glyphicon glyphicon-home"></i> Condomínio')}" />
												
												<g:sortableColumn property="recurso" title="${message(code: 'reserva.recurso.label', default: '<i class="glyphicon glyphicon-list"></i> Recurso')}" />
																	
												<g:sortableColumn property="dataInicioEvento" title="${message(code: 'reserva.dataInicioEvento.label', default: '<i class="glyphicon glyphicon-calendar"></i> Início do evento')}" />
												
												<g:sortableColumn property="dataFimEvento" title="${message(code: 'reserva.dataFimEvento.label', default: '<i class="glyphicon glyphicon-calendar"></i> Fim do evento')}" />
											
												<th><i class="glyphicon glyphicon-ok-sign"></i> <g:message code="reserva.status.label" default="Status" /></th>
											
												<th><i class="glyphicon glyphicon-home"></i> <g:message code="reserva.apartamento.label" default="Apartamento" /></th>
											
											</tr>
										</thead>
										<tbody>
										<g:each in="${reservaInstanceList}" status="i" var="reservaInstance">
											<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
											
												<td><g:link controller="condominio" action="show" id="${reservaInstance.recurso.condominio.id}">${fieldValue(bean: reservaInstance, field: "recurso.condominio")}</g:link></td>
												
												<td><g:link controller="recurso" action="show" id="${reservaInstance.recurso.id}">${fieldValue(bean: reservaInstance, field: "recurso")}</g:link></td>
																							
												<td><g:link controller="reserva" action="show" id="${reservaInstance.id}"><g:formatDate date="${reservaInstance.dataInicioEvento}" format="dd/MM/yyyy HH:mm:ss" /></g:link></td>
												
												<td><g:link controller="reserva" action="show" id="${reservaInstance.id}"><g:formatDate date="${reservaInstance.dataFimEvento}" format="dd/MM/yyyy HH:mm:ss" /></g:link></td>
											
												<td>
													<g:if test="${reservaInstance.aprovada && reservaInstance.dataAprovacao}">
														<g:message code="reserva.aprovada.label" default="Aprovada" />
													</g:if>
													<g:else>
														<g:if test="${reservaInstance.cancelada && reservaInstance.dataCancelamento}">
															<g:message code="reserva.cancelada.label" default="Cancelada" />
														</g:if>
														<g:else>
															<g:message code="reserva.pendente.label" default="Pendente" />
															<g:if test="${(usuario.id != reservaInstance.usuario.id)}">
																<div class="form-inline">
																	<div class="form-group">
																		<g:formRemote name="reserva_form" url="[controller:'reserva', action:'atualizarReserva']" update="minhasReservas">
																			<g:hiddenField name="id" value="${reservaInstance.id}"/>
																			<g:hiddenField name="aprovada" value="1"/>
																			<g:hiddenField name="cancelada" value="1"/>		
																			<button type="submit" class="btn btn-success" id="aprovarReserva" name="aprovarReserva">
																		    	<i class="glyphicon glyphicon-ok"></i> Aprovar
																		    </button>							
																		</g:formRemote>
																	</div>	
																	
																	<div class="form-group">
																		<g:formRemote name="reserva_form" url="[controller:'reserva', action:'atualizarReserva']" update="minhasReservas">
																			<g:hiddenField name="id" value="${reservaInstance.id}"/>																			
																			<g:hiddenField name="aprovada" value="0"/>
																			<g:hiddenField name="cancelada" value="1"/>	
																			<button type="submit" class="btn btn-danger" id="negarReserva" name="negarReserva">
																		    	<i class="glyphicon glyphicon-remove"></i> Cancelar
																		    </button>
																		</g:formRemote>
																	</div>											
																</div>
															</g:if>
														</g:else>
													</g:else>							
												</td>
																						
												<td><g:link controller="apartamento" action="show" id="${reservaInstance.apartamento.id}">${fieldValue(bean: reservaInstance, field: "apartamento")}</g:link></td>
											
											</tr>
										</g:each>
										</tbody>
									</table>
								</div>
							
								<div class="row text-center">
<%--									<div class="pagination">--%>
<%--										<g:paginate total="${reservaInstanceTotal}" params="['recurso.id':1, 'condominio.id':2, 'reserva':3, 'status_reserva':4, 'dataInicio':5, 'dataFim':6]" />--%>
<%--									</div>--%>
									
									<div class="paginateButtons">
									    <util:remotePaginate total="${reservaInstanceTotal}" update="minhasReservas" controller="reserva" action="listPaginate" />
									</div>
								</div>	
							</g:if>
							<g:else>
								<div class="alert alert-info">
									<i class="glyphicon glyphicon-alert"></i> <g:message code="condominio.nenhum.resultado.label" default="Nenhum resultado foi encontrado" />
								</div>
							</g:else>
						</div>
					</div>
				</div>
			</div>	
						
			