
<%@ page import="br.com.reservas.Reserva" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'reserva.label', default: 'Reserva')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-reserva" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<ol class="breadcrumb" >
			<li>
				<a href="${createLink(uri: '/')}">
					<i class="glyphicon glyphicon-home"></i> <%-- <g:message code="default.home.label"/> --%>
				</a>
			</li>
			<sec:ifAnyGranted roles="ROLE_ADMIN">
				<li>
					<g:link class="create" action="create">
						<i class="glyphicon glyphicon-plus"></i> <g:message code="default.new.label" args="[entityName]" />
					</g:link>
				</li>
			</sec:ifAnyGranted>	
			<li>
				<g:link class="list" controller="condominio" action="list">
					<i class="glyphicon glyphicon-list"></i> <g:message code="default.list.reservas.label" default="Lista de Condomínios" />
				</g:link>
			</li>
		</ol>
		
		<div id="list-reserva" class="container-fluid" role="main">
			<h1><g:message code="default.list.reservas.label" default="Lista de Reservas" /></h1>
			
			
			<g:formRemote name="search" url="[action:'search']" update="searchResult">
				<div class="panel panel-default">
					<div class="panel-heading">
						<g:message code="default.list.reservas.label" default="Lista de Reservas" />
					</div>
					<div class="panel-body">						
						<div class="row">
							<div class="form-group col-sm-3">	
								<label for="condominio">
									<g:message code="condominio.label" default="Condomínio" />
									<span class="required-indicator">*</span>
								</label>
								<g:select id="condominio" name="condominio.id" from="${condominioInstanceListSelect}" optionKey="id" required="" value="0" class="form-control many-to-one" onchange="${remoteFunction(controller: 'recurso', action: 'buscaRecursos', params: '\'condominio=\' + this.value', update:'recursoSelect')}" />
							</div>
							<div class="form-group col-sm-3">	
								<label for="recurso">
									<g:message code="condominio.recurso.label" default="Recurso" />
									<span class="required-indicator">*</span>
								</label>
								<div id="recursoSelect" style="display: inline;">
									<g:render template="/recurso/recurso" model="[recursoInstanceList: recursoInstanceList]" ></g:render>
								</div>
							</div>	
							
							<div class="form-group col-sm-3">
								<label for="reserva">
									<g:message code="condominio.reserva.label" default="Reserva" />
									<span class="required-indicator">*</span>
								</label>
								<g:select id="reserva" name="reserva" from="${[message(code: 'minhas.reservas.label', default: 'Minhas reservas'), message(code: 'todas.reservas.label', default: 'Todas as reservas')]}" keys="${[0, 1]}" required="" value="0" class="form-control many-to-one"/>
							</div>
							
							<div class="form-group col-sm-3">	
								<label for="status">
									<g:message code="condominio.reserva.status.label" default="Status" />
								</label>
								<g:select id="status_reserva" name="status_reserva" from="${[message(code: 'todos.label', default: 'Todos'), message(code: 'aprovada.label', default: 'Aprovada'), message(code: 'cancelada.label', default: 'Cancelada'), message(code: 'pendente.label', default: 'Pendente')]}" keys="${[0, 1, 2, 3]}" required="" value="0" class="form-control many-to-one"/>
							</div>
						</div>	
						<div class="row">
							<div class="form-group col-sm-3">	
								<label for="dataInicio">
									<g:message code="condominio.reserva.data.inicio.label" default="A partir de" />
								</label>
								<div class="datepicker-day">
									<g:datePicker name="dataInicio" precision="day"/>
								</div>	
							</div>
							<div class="form-group col-sm-3">	
								<label for="dataFim">
									<g:message code="condominio.reserva.data.fim.label" default="Até" />
								</label>
								<div class="datepicker-day">
									<g:datePicker name="dataFim" precision="day" value="${dataFim}"/>
								</div>	
							</div>
						</div>
					</div>		
					<div class="panel-footer">
						<div class="row">
							<div class="col-sm-12">
								<div class="pull-right">				
							    	<button type="submit" class="btn btn-primary" id="search" name="search">
					                	<i class="glyphicon glyphicon-search"></i> Buscar
					                </button>
							    </div>
							</div>    
					    </div> 	
			        </div>			        
				</div>        
	    	</g:formRemote>
			
			<div class="row">
				<div class="form-group col-sm-12">
					<div id="searchResult">
						<g:render template="list"/>
					</div>
				</div>	
			</div>		
		</div>
		
	</body>
</html>
