
<%@ page import="br.com.reservas.Reserva" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'reserva.label', default: 'Reserva')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-reserva" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<ol class="breadcrumb">
			<li>
				<a class="home" href="${createLink(uri: '/')}">
					<i class="glyphicon glyphicon-home"></i> <g:message code="default.home.label"/>
				</a>
			</li>
		</ol>

		<div id="show-reserva" class="container-fluid" role="main">		
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> ${flash.message}</div>
			</g:if>
			
			<div class="panel panel-default">
				<div class="panel-heading">
					<g:message code="default.show.label" args="[entityName]" />
				</div>
				
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-6">
							<g:if test="${reservaInstance?.recurso?.condominio}">
								<label for="condominio"><i class="glyphicon glyphicon-home"></i> <g:message code="reserva.condominio.label" default="Nome do condomínio" /></label>
								<div>
									<g:link controller="condominio" action="show" id="${reservaInstance?.recurso?.condominio?.id}">${reservaInstance?.recurso?.condominio?.encodeAsHTML()}</g:link>
								</div>	
							</g:if>							
						</div>
						
						<div class="col-sm-6">
							<g:if test="${reservaInstance?.recurso}">								
								<label for="recurso"><i class="glyphicon glyphicon-blackboard"></i> <g:message code="reserva.recurso.label" default="Recurso" /></label>
								<div>
									<g:link controller="recurso" action="show" id="${reservaInstance?.recurso?.id}">${reservaInstance?.recurso?.encodeAsHTML()}</g:link>
								</div>	
							</g:if>
						</div>
					</div>	
					
					<hr>
					
					<div class="row">
						<div class="col-sm-4">
							<g:if test="${reservaInstance?.dataInicioEvento}">
								<label for="dataInicioEvento"><i class="glyphicon glyphicon-calendar"></i> <g:message code="reserva.dataInicioEvento.label" default="Início do evento" /></label>
								<g:if test="${reservaInstance?.recurso.unidadeTempoReserva.id == 3}">
									<div>
										<g:formatDate format="dd/MM/yyyy" date="${reservaInstance?.dataInicioEvento}" />
									</div>	
								</g:if>
								<g:else>
									<div>
										<g:formatDate format="dd/MM/yyyy HH:mm:ss" date="${reservaInstance?.dataInicioEvento}" />
									</div>	
								</g:else>
							</g:if>
						</div>
						
						<div class="col-sm-4">	
							<g:if test="${reservaInstance?.dataFimEvento}">
								<label for="dataFimEvento"><i class="glyphicon glyphicon-calendar"></i> <g:message code="reserva.dataFimEvento.label" default="Fim do evento" /></label>
								<g:if test="${reservaInstance?.recurso.unidadeTempoReserva.id == 3}">
									<div>
										<g:formatDate format="dd/MM/yyyy" date="${reservaInstance?.dataFimEvento}" />
									</div>	
								</g:if>
								<g:else>
									<div>
										<g:formatDate format="dd/MM/yyyy HH:mm:ss" date="${reservaInstance?.dataFimEvento}" />
									</div>	
								</g:else>
							</g:if>
						</div>
						
						<div class="col-sm-4">	
							<g:if test="${reservaInstance?.dataSolicitacao}">
								<label for="dataSolicitacao"><i class="glyphicon glyphicon-calendar"></i> <g:message code="reserva.dataSolicitacao.label" default="Data da Solicitação" /></label>
								<div>
									<g:formatDate format="dd/MM/yyyy HH:mm:ss" date="${reservaInstance?.dataSolicitacao}" />
								</div>	
							</g:if>
						</div>
					</div>
					
					<hr>
					
					<div class="row">
						<div class="col-sm-4">
							<g:if test="${reservaInstance?.valor}">
								<label for="valor"><i class="glyphicon glyphicon-usd"></i> <g:message code="reserva.valor.label" default="Valor" /></label>
								<div>
									<g:formatNumber number="${reservaInstance?.valor}" type="currency" currencyCode="BRL" />
								</div>	
							</g:if>
						</div>
						
						<div class="col-sm-4">	
							<g:if test="${reservaInstance?.aprovada}">
								<label for="dataAprovacao"><i class="glyphicon glyphicon-calendar"></i> <g:message code="reserva.dataAprovacao.label" default="Data da aprovação" /></label>
								<div>
									<g:formatDate date="${reservaInstance?.dataAprovacao}" />
								</div>	
							</g:if>
							<g:else>
								<label for="aprovada"><i class="glyphicon glyphicon-ok-sign"></i> <g:message code="reserva.aprovada.label" default="Aprovada" /></label>
								<div>
									<g:message code="reserva.aprovada.label" default="Não" />
								</div>	
							</g:else>
						</div>
						
						<div class="col-sm-4">	
							<g:if test="${reservaInstance?.cancelada}">
								<label for="dataCancelamento"><i class="glyphicon glyphicon-calendar"></i> <g:message code="reserva.dataCancelamento.label" default="Data do cancelamento" /></label>
								<div>
									<g:formatDate date="${reservaInstance?.dataCancelamento}" />
								</div>	
							</g:if>
							<g:else>
								<label for="cancelada"><i class="glyphicon glyphicon-ok-sign"></i> <g:message code="reserva.cancelada.label" default="Cancelada" /></label>
								<div>
									<g:message code="reserva.cancelada.label" default="Não" />
								</div>	
							</g:else>	
						</div>
					</div>
					
					<hr>
					
					<div class="row">
						<div class="col-sm-4">
							<g:if test="${reservaInstance?.apartamento}">
								<label for="apartamento"><i class="glyphicon glyphicon-home"></i> <g:message code="reserva.apartamento.label" default="Apartamento" /></label>
								<div>
									<g:link controller="apartamento" action="show" id="${reservaInstance?.apartamento?.id}">${reservaInstance?.apartamento?.encodeAsHTML()}</g:link>
								</div>	
							</g:if>
						</div>	
								
						<div class="col-sm-4">							
							<g:if test="${reservaInstance?.usuario}">
								<label for="usuario"><i class="glyphicon glyphicon-user"></i> <g:message code="reserva.usuario.label" default="Usuário" /></label>
								<div>
									<g:link controller="usuario" action="show" id="${reservaInstance?.usuario?.id}">${reservaInstance?.usuario?.encodeAsHTML()}</g:link>
								</div>	
							</g:if>
						</div>
						
						<div class="col-sm-4">	
							<g:if test="${reservaInstance?.comentario}">
								<label for="comentario"><i class="glyphicon glyphicon-info"></i> <g:message code="reserva.comentario.label" default="Comentário" /></label>
								<div>
									<g:fieldValue bean="${reservaInstance}" field="comentario"/>
								</div>	
							</g:if>
						</div>
					</div>
					
					<g:if test="${ehAdministrador}">
						<hr>
						
						<div class="row">
							<div class="col-sm-12">
								<label for="convidados"><i class="glyphicon glyphicon-list"></i> <g:message code="recurso.convidados.label" default="Convidados" /></label>
								
								<g:render template="/convidado/list"/>
								
								<div class="pull-right">
									<g:if test="${reservaInstance?.id}">
										<g:link class="btn btn-default" controller="convidado" action="create" params="['reserva.id': reservaInstance?.id]"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'convidado.label', default: 'Convidado')])}</g:link>
									</g:if>
									<g:else>
										<div class="btn btn-default disabled"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'convidado.label', default: 'Convidado')])}</div>				
									</g:else>						
								</div>	
							</div>
						</div>
					</g:if>	
						
				</div>
				
				<g:if test="${ehAdministrador}">
					<div class="panel-footer">
						<div class="row">
							<div class="col-sm-12">
								<div class="pull-right">								
									<g:form action="delete">
										<fieldset class="buttons">
											<g:hiddenField name="id" value="${reservaInstance?.id}" />
											<g:link class="btn btn-primary" action="edit" id="${reservaInstance?.id}"><i class="glyphicon glyphicon-pencil"></i> <g:message code="default.button.edit.label" default="Edit" /></g:link>
											<button type="submit" class="btn btn-primary" id="delete" name="delete" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Tem certeza?')}');">
								            	<i class="glyphicon glyphicon-remove"></i> ${message(code: 'default.button.delete.label', default: 'Delete')}
								            </button>
										</fieldset>
									</g:form>
								</div>
							</div>
						</div>
					</div>
				</g:if>
			</div>		
			
		</div>
	</body>
</html>
