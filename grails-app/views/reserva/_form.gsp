<%@ page import="br.com.reservas.Reserva" %>

<div class="row">
	<div class="col-sm-4">
		<div class="form-group ${hasErrors(bean: recursoInstance, field: 'condominio', 'error')}">
			<label for="condominio">
				<i class="glyphicon glyphicon-home"></i> <g:message code="reserva.condominio.label" default="Nome do condomínio" />
			</label>
			<h2><g:link controller="condominio" action="show" id="${recursoInstance?.condominio?.id}">${recursoInstance?.condominio?.encodeAsHTML()}</g:link></h2>
		</div>
	</div>
	
	<div class="col-sm-4">	
		<div class="form-group ${hasErrors(bean: recursoInstance, field: 'recurso', 'error')}">
			<label for="recurso">
				<i class="glyphicon glyphicon-blackboard"></i> <g:message code="reserva.recurso.label" default="Recurso" />
			</label>
			
			<div>
			<g:link controller="recurso" action="show" id="${recursoInstance?.id}">${recursoInstance?.encodeAsHTML()}</g:link>
			</div>
			<g:hiddenField id="recurso" name="recurso.id" value="${reservaInstance?.recurso?.id}"/>
		</div>
	</div>
</div>		

<hr>

<div class="row">
	<div class="col-sm-3">
		<div class="form-group ${hasErrors(bean: reservaInstance, field: 'apartamento', 'error')} required">
			<label for="apartamento">
				<i class="glyphicon glyphicon-list"></i> <g:message code="reserva.apartamento.label" default="Apartamento" />
				<span class="required-indicator">*</span>
			</label>
			<g:select id="apartamento" name="apartamento.id" from="${apartamentoInstanceList}" optionKey="id" required="" value="${reservaInstance?.apartamento?.id}" class="form-control many-to-one" noSelection="['':'Selecione um apartamento...']"/>
		</div>
	</div>	
	
	<div class="col-sm-3">
		<div class="form-group ${hasErrors(bean: reservaInstance, field: 'valor', 'error')} required">
			<label for="valor">
				<i class="glyphicon glyphicon-usd"></i> <g:message code="reserva.valor.label" default="Valor" />
			</label>
		
			<div>
			<g:if test="${action != 'create'}">
				<g:hiddenField name="valor" value="${fieldValue(bean: reservaInstance, field: 'valor')}"/>
				<g:formatNumber number="${reservaInstance?.valor}" type="currency" currencyCode="BRL" />
			</g:if>
			<g:else>
				<g:hiddenField name="valor" value="${fieldValue(bean: recursoInstance, field: 'valor')}"/>
				<g:formatNumber number="${recursoInstance?.valor}" type="currency" currencyCode="BRL" />
			</g:else>
			</div>		
		</div>
	</div>
	
	<g:hiddenField name="dataSolicitacao" value="${reservaInstance?.dataSolicitacao}"  />

	<div class="col-sm-3">
		<div class="form-group ${hasErrors(bean: reservaInstance, field: 'dataInicioEvento', 'error')} required">
			<g:if test="${reservaInstance?.id}">
				<label for="dataInicioEvento">
					<i class="glyphicon glyphicon-calendar"></i> <g:message code="reserva.dataInicioEvento.label" default="Início do evento" />
					<span class="required-indicator">*</span>
				</label>
				<div class="datepicker-${precision}">
					<g:datePicker name="dataInicioEvento" precision="${precision}"  value="${reservaInstance?.dataInicioEvento}"  />
				</div>	
			</g:if>
			<g:else>
				<label for="dataInicioEvento">
					<i class="glyphicon glyphicon-calendar"></i> <g:message code="reserva.dataInicioEvento.label" default="Início do evento" />
				</label>
				<div>
				<g:if test="${recursoInstance?.unidadeTempoReserva?.id == 3}">
					<span id="dataInicioEvento_span">
						<g:formatDate date="${reservaInstance?.dataInicioEvento}" format="dd/MM/yyyy"/>
					</span>	
				</g:if>
				<g:else>
					<span id="dataInicioEvento_span">
						<g:formatDate date="${reservaInstance?.dataInicioEvento}" format="dd/MM/yyyy HH:mm:ss"/>
					</span>
				</g:else>
				</div>		 
				<g:hiddenField name="dataInicioEvento" value="${reservaInstance?.dataInicioEvento}"  />
				<g:hiddenField name="dataInicioEvento_day" value="${diaEvento}"  />
				<g:hiddenField name="dataInicioEvento_year" value="${anoEvento}"  />
				<g:hiddenField name="dataInicioEvento_month" value="${mesEvento}"  />
				<g:hiddenField name="dataInicioEvento_hour" value="${horaEvento}"  />
				<g:hiddenField name="dataInicioEvento_minute" value="${minutoEvento}"  />
				<g:hiddenField name="dataInicioEvento_second" value="${segundoEvento}"  />
			</g:else>
		</div>
	</div>
	
	<div class="col-sm-3">	
		<div class="form-group ${hasErrors(bean: reservaInstance, field: 'dataFimEvento', 'error')} required">
			<g:if test="${reservaInstance?.id}">
				<label for="dataFimEvento">
					<i class="glyphicon glyphicon-calendar"></i> <g:message code="reserva.dataFimEvento.label" default="Fim do evento" />
					<span class="required-indicator">*</span>
				</label>
				<div class="datepicker-${precision}">
					<g:datePicker name="dataFimEvento" precision="${precision}"  value="${reservaInstance?.dataFimEvento}"  />
				</div>	
			</g:if>
			<g:else>
				<label for="dataFimEvento">
					<i class="glyphicon glyphicon-calendar"></i> <g:message code="reserva.dataFimEvento.label" default="Fim do evento" />
				</label>
				<div>
				<g:if test="${recursoInstance?.unidadeTempoReserva?.id == 3}">
					<span id="dataFimEvento_span">
						<g:formatDate date="${reservaInstance?.dataFimEvento}" format="dd/MM/yyyy"/>
					</span>	
				</g:if>
				<g:else>
					<span id="dataFimEvento_span">
						<g:formatDate date="${reservaInstance?.dataFimEvento}" format="dd/MM/yyyy HH:mm:ss"/>
					</span>	
				</g:else>
				</div>		 
				<g:hiddenField name="dataFimEvento" value="${reservaInstance?.dataFimEvento}"  />
				<g:hiddenField name="dataFimEvento_day" value="${diaEvento}"  />
				<g:hiddenField name="dataFimEvento_year" value="${anoEvento}"  />
				<g:hiddenField name="dataFimEvento_month" value="${mesEvento}"  />
				<g:hiddenField name="dataFimEvento_hour" value="${horaEvento}"  />
				<g:hiddenField name="dataFimEvento_minute" value="${minutoEvento}"  />
				<g:hiddenField name="dataFimEvento_second" value="${segundoEvento}"  />
			</g:else>
		</div>
	</div>
</div>		

<g:if test="${action != 'create'}">
	<div class="row">
		<div class="col-sm-4">
			<div class="form-group ${hasErrors(bean: reservaInstance, field: 'dataAprovacao', 'error')}">
				<label for="dataAprovacao">
					<i class="glyphicon glyphicon-date"></i> <g:message code="reserva.dataAprovacao.label" default="Data da aprovação" />
				</label>
				<div class="datepicker-day">
				<g:datePicker name="dataAprovacao" precision="day"  value="${reservaInstance?.dataAprovacao}"  />
				</div>
			</div>
			
			<div class="form-group ${hasErrors(bean: reservaInstance, field: 'aprovada', 'error')} ">
				<label for="aprovada">
					<i class="glyphicon glyphicon-ok-sign"></i> <g:message code="reserva.aprovada.label" default="Aprovada" />
					
				</label>
				<div>
				<g:checkBox name="aprovada" value="${reservaInstance?.aprovada}" />
				</div>
			</div>
		</div>			

		<div class="col-sm-3">
			<div class="form-group ${hasErrors(bean: reservaInstance, field: 'dataCancelamento', 'error')}">
				<label for="dataCancelamento">
					<i class="glyphicon glyphicon-ban-circle"></i> <g:message code="reserva.dataCancelamento.label" default="Data do cancelamento" />
				</label>
				<div class="datepicker-day">
				<g:datePicker name="dataCancelamento" precision="day"  value="${reservaInstance?.dataCancelamento}"  />
				</div>
			</div>
		
			<div class="form-group ${hasErrors(bean: reservaInstance, field: 'cancelada', 'error')} ">
				<label for="cancelada">
					<i class="glyphicon glyphicon-ban-circle"></i> <g:message code="reserva.cancelada.label" default="Cancelada" />
					
				</label>
				<div>
				<g:checkBox name="cancelada" value="${reservaInstance?.cancelada}" />
				</div>
			</div>
		</div>
	</div>		
</g:if>	
<g:else>
	<g:hiddenField name="cancelada" value="false" />
	<g:hiddenField name="aprovada" value="false" />
</g:else>

<div class="row">
	<div class="col-sm-12">
		<div class="form-group ${hasErrors(bean: reservaInstance, field: 'comentario', 'error')} ">
			<label for="comentario">
				<i class="glyphicon glyphicon-info-sign"></i> <g:message code="reserva.comentario.label" default="Comentário" />
				
			</label>
			<g:textArea name="comentario" value="${reservaInstance?.comentario}" class="form-control"/>
		</div>
	</div>
</div>		

<hr>

<div class="row">
	<div class="col-sm-12">
		<div class="form-group ${hasErrors(bean: reservaInstance, field: 'convidados', 'error')} ">
			<label for="convidados">
				<i class="glyphicon glyphicon-user"></i> <g:message code="reserva.convidados.label" default="Convidados" />
			</label>
			<div>
			<div class="pull-right">	
				<g:if test="${reservaInstance?.id}">
					<g:link class="btn btn-default" controller="convidado" action="create" params="['reserva.id': reservaInstance?.id]"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'convidado.label', default: 'Convidados')])}</g:link>
				</g:if>
				<g:else>
					<div class="btn btn-default disabled"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'convidado.label', default: 'Convidados')])}</div>				
				</g:else>	
				</div>
			</div>	
		</div>
	</div>
</div>		

<hr>

<g:hiddenField  id="usuario" name="usuario.id" value="${reservaInstance?.usuario?.id}" />

<g:render template="/recurso/calendar" model="['dataInicioEvento':reservaInstance?.formatarData(reservaInstance?.dataInicioEvento)]" />
