<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="reservas-online"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
				
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
			
		<!-- jQuery library -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
			
		<!-- Latest compiled JavaScript -->
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'main.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'mobile.css')}" type="text/css">
		
		<g:layoutHead/>
		<r:layoutResources />
	</head>
	<body>
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid login-header">
				<div class="row">
					<div class="col-sm-12">
						<div class="pull-right" id="loginHeader"><g:loginControl /></div>
					</div>
				</div>
			</div>	
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target="#myNavbar">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="${createLink(uri: '/')}"><span class="glyphicons glyphicons-truck"></span> <i class="glyphicon glyphicon-calendar"></i> Reservas Online</a>
				</div>
				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="${createLink(uri: '/')}#voce">Para você</a></li>
						<li><a href="${createLink(uri: '/')}#condominio">Para seu condomínio</a></li>
						<li><a href="${createLink(uri: '/')}#planos">Planos</a></li>
						<li><a href="${createLink(uri: '/')}#contato">Contato</a></li>
					</ul>
				</div>
			</div>
		</nav>

<%--		<header class="page-head">--%>
<%--			<div class="jumbotron text-center">--%>
<%--				<i class="glyphicon glyphicon-calendar logo"></i>--%>
<%--		  		<h1>Reservas Online</h1>--%>
<%--		  		<p>Reserve seu evento de forma rápida e pratica</p>		  		--%>
<%--			</div>--%>
<%--		</header>--%>
	
		<g:layoutBody/>

		<footer class="page-foot">
			<div class="jumbotron text-center">
				<p>Copyright</p>
			</div>
		</footer>
	
		<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
		<g:javascript library="application"/>
		<r:layoutResources />
	</body>
</html>
