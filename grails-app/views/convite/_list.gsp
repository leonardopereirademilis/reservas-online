<h1><g:message code="default.convites.pendentes.label" default="Convites pendentes" args="[entityName]" /></h1>
<div class="panel panel-default">
   	<div class="panel-heading"><g:message code="default.convites.pendentes.label" default="Convites pendentes" args="[entityName]" /></div>
   	<div class="panel-body">
   		<g:if test="${convitesPendentesList.size() > 0}">
			<table class="table table-bordered table-striped">
				<thead>
					<th><i class="glyphicon glyphicon-user"></i> <g:message code="convite.condominio.label" default="Usuário" /></th>
					<th><i class="glyphicon glyphicon-home"></i> <g:message code="convite.condominio.label" default="Condomínio" /></th>
					<th><i class="glyphicon glyphicon-map-marker"></i> <g:message code="convite.endereco.label" default="Endereço" /></th>
					<th><i class="glyphicon glyphicon-list"></i> <g:message code="convite.apartamento.label" default="Apartamento" /></th>
					<th><i class="glyphicon glyphicon-calendar"></i> <g:message code="convite.data.solicitacao.label" default="Data da Solicitação" /></th>
					<th><i class="glyphicon glyphicon-cog"></i> <g:message code="convite.acao.label" default="Ação" /></th>
				</thead>
				<tbody>
					<g:each in="${convitesPendentesList}" var="c">
						<tr>
							<td>
								<g:link controller="usuario" action="show" id="${c.usuario.id}">${c.usuario}</g:link>
							</td>
							<td>
								<g:link controller="condominio" action="show" id="${c.apartamento.condominio.id}">
									<g:fieldValue bean="${c}" field="apartamento.condominio"/>
								</g:link>
							</td>
							<td>
								${c.apartamento.condominio.endereco.toHTML()}
							</td>
							<td>
								<g:fieldValue bean="${c}" field="apartamento"/>
							</td>								
							<td>	
								<g:formatDate date="${c.dataConvite}" format="dd/MM/yyyy HH:mm:ss"/>
							</td>
							<td>
								<g:if test="${((usuario.id == c.usuario.id) && (!c.usuarioSolicitou)) || ((usuario.id != c.usuario.id) && (c.usuarioSolicitou))}">
									<div class="form-inline">
										<div class="form-group">
											<g:formRemote name="permissao_form" url="[controller:'convite', action:'atualizarConvite']" update="meusCondominioseConvites">
												<g:hiddenField name="id" value="${c.id}"/>
												<g:hiddenField name="condominioInstanceId" value="${c.apartamento.condominio.id}"/>
												<g:hiddenField name="aprovado" value="1"/>		
												<g:hiddenField name="template" value="list"/>					
												<button type="submit" class="btn btn-success" id="aceitarConvite" name="aceitarConvite">
											    	<i class="glyphicon glyphicon-ok"></i> Sim
											    </button>							
											</g:formRemote>
										</div>	
										
										<div class="form-group">
											<g:formRemote name="permissao_form" url="[controller:'convite', action:'atualizarConvite']" update="meusCondominioseConvites">
												<g:hiddenField name="id" value="${c.id}"/>
												<g:hiddenField name="condominioInstanceId" value="${c.apartamento.condominio.id}"/>
												<g:hiddenField name="aprovado" value="0"/>	
												<g:hiddenField name="template" value="list"/>											
												<button type="submit" class="btn btn-danger" id="negarConvite" name="negarConvite">
											    	<i class="glyphicon glyphicon-remove"></i> Não
											    </button>
											</g:formRemote>
										</div>											
									</div>
								</g:if>
								<g:else>									
									<g:if test="${(usuario.id == c.usuario.id)}">
										<g:message code="condominio.aguardando.label" default="Aguardando aceite do Administrador" />
									</g:if>
									<g:else>
										<g:message code="condominio.aguardando.label" default="Aguardando aceite do Usuário" />
									</g:else>
									<div>
										<g:formRemote name="permissao_form" url="[controller:'convite', action:'atualizarConvite']" update="meusCondominios">
											<g:hiddenField name="id" value="${c.id}"/>
											<g:hiddenField name="condominioInstanceId" value="${c.apartamento.condominio.id}"/>
											<g:hiddenField name="aprovado" value="0"/>	
											<g:hiddenField name="template" value="list"/>											
											<button type="submit" class="btn btn-danger" id="negarConvite" name="negarConvite">
											   	<i class="glyphicon glyphicon-ban-circle"></i> Cancelar
											   </button>
										</g:formRemote>									
									</div>	
								</g:else>	
							</td>					
						</tr>			
					</g:each>
				</tbody>	
			</table>
		</g:if>
		<g:else>
			<div class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> 
				<g:message code="condominio.nenhum.resultado.label" default="Nenhum resultado foi encontrado" />
			</div>
		</g:else>	
	</div>
</div>		