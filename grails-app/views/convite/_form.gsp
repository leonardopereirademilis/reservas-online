<%@ page import="br.com.reservas.Convite" %>


<div class="row">
	<div class="col-sm-4">
		
		<g:if test="${conviteInstance.apartamento?.condominio?.id}">
			<div class="form-group ${hasErrors(bean: condominioInstance, field: 'condominio', 'error')} ">
				<label for="condominio">
					<i class="glyphicon glyphicon-home"></i> <g:message code="convite.condominio.label" default="Nome do condomínio" />
					
				</label>
					
				<g:link controller="condominio" action="show" id="${conviteInstance.apartamento?.condominio?.id}"><g:textField class="form-control" name="email" disabled value="${conviteInstance.apartamento?.condominio?.encodeAsHTML()}"/></g:link>
					
			</div>
		</g:if>
	</div>
	
	<div class="col-sm-4">	
		<div class="form-group ${hasErrors(bean: conviteInstance, field: 'email', 'error')} required">
			<label for="email">
				<i class="glyphicon glyphicon-envelope"></i> <g:message code="convite.email.label" default="E-mail" />
				<span class="required-indicator">*</span>
			</label>
			<g:textField class="form-control" name="email" required="" value="${conviteInstance?.email}"/>
		</div>
	</div>
	
	<div class="col-sm-4">	
		<div class="form-group ${hasErrors(bean: conviteInstance, field: 'apartamento', 'error')} required">
			<label for="apartamento">
				<i class="glyphicon glyphicon-list"></i> <g:message code="convite.apartamento.label" default="Apartamento" />
			</label>
			
			<g:link controller="apartamento" action="show" id="${conviteInstance?.apartamento?.id}"><g:textField class="form-control" name="email" disabled value="${conviteInstance?.apartamento}"/> </g:link>				
			<g:hiddenField id="apartamento" name="apartamento.id" value="${conviteInstance?.apartamento?.id}"/>
		</div>
	</div>	
</div>	

<hr>

<div class="row">
	<div class="col-sm-4">
		<div class="form-group ${hasErrors(bean: conviteInstance, field: 'dataConvite', 'error')}">
			<label for="dataConvite">
				<i class="glyphicon glyphicon-calendar"></i> <g:message code="convite.dataConvite.label" default="Data do convite" />		
			</label>
			
			<g:if test="${conviteInstance?.dataConvite}">
				<g:textField class="form-control" name="email" disabled value="${conviteInstance?.formatarData(conviteInstance?.dataConvite)}"/>
				<g:hiddenField name="dataConvite" value="${conviteInstance?.dataConvite}"  />
			</g:if>
			<g:else>
				<g:textField class="form-control" name="email" disabled value="${dataAtual}"/>
				<g:hiddenField name="dataConvite" value=""  />
			</g:else>
					
		</div>
	</div>
	
	<div class="col-sm-4">	
		<g:if test="${conviteInstance?.dataAceite }">
			<div class="form-group ${hasErrors(bean: conviteInstance, field: 'dataAceite', 'error')} ">
				<label for="dataAceite">
					<i class="glyphicon glyphicon-calendar"></i> <g:message code="convite.dataAceite.label" default="Data do aceite" />
					
				</label>
				<div class="datepicker-day">
					<g:datePicker name="dataAceite" precision="day"  value="${conviteInstance?.dataAceite}" default="none" noSelection="['': '']" />
				</div>	
			</div>
		</g:if>
	</div>
</div>		