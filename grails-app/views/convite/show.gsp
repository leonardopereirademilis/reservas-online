
<%@ page import="br.com.reservas.Convite" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'convite.label', default: 'Convite')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-convite" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<ol class="breadcrumb">
			<li><a class="home" href="${createLink(uri: '/')}"><i class="glyphicon glyphicon-home"></i> <g:message code="default.home.label"/></a></li>
			<li><g:link class="list" action="list"><i class="glyphicon glyphicon-list"></i> <g:message code="default.list.label" args="[entityName]" /></g:link></li>
			<li><g:link class="create" action="create"><i class="glyphicon glyphicon-plus"></i> <g:message code="default.new.label" args="[entityName]" /></g:link></li>
		</ol>
		<div id="show-convite" class="container-fluid" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> ${flash.message}</div>
			</g:if>
			<div class="panel panel-default">
				<div class="panel-heading">
					<g:message code="default.show.label" args="[entityName]" />
				</div>
				<div class="panel-body">
					<div class="row">
						<g:if test="${conviteInstance?.usuario}">
							<div class="col-sm-4">
								<div class="form-group">
									<label for="usuario"><i class="glyphicon glyphicon-user"></i> <g:message code="convite.usuario.label" default="Usuário" /></label>
									
									<div><g:link controller="usuario" action="show" id="${conviteInstance?.usuario?.id}">${conviteInstance?.usuario?.encodeAsHTML()}</g:link></div>
								</div>
							</div>		
						</g:if>
														
						<g:if test="${conviteInstance?.email}">
							<div class="col-sm-4">
								<div class="form-group">
									<label for="email"><i class="glyphicon glyphicon-envelope"></i> <g:message code="convite.email.label" default="E-mail" /></label>
									
									<div><g:fieldValue bean="${conviteInstance}" field="email"/></div>
								</div>
							</div>									
						</g:if>
							
						<g:if test="${conviteInstance?.apartamento}">
							<div class="col-sm-4">
								<div class="form-group">
									<label for="apartamento"><i class="glyphicon glyphicon-home"></i> <g:message code="convite.apartamento.label" default="Apartamento" /></label>
									
									<div><g:link controller="apartamento" action="show" id="${conviteInstance?.apartamento?.id}">${conviteInstance?.apartamento?.encodeAsHTML()}</g:link></div>
									
								</div>
							</div>	
						</g:if>
					</div>
				
					<hr>
				
					<div class="row">						
						<g:if test="${conviteInstance?.dataConvite}">
							<div class="col-sm-4">
								<div class="form-group">
									<label for="dataConvite"><i class="glyphicon glyphicon-calendar"></i> <g:message code="convite.dataConvite.label" default="Data do convite" /></label>
							
									<div><g:formatDate date="${conviteInstance?.dataConvite}" format="dd/MM/yyyy HH:mm:ss" /></div>
							
								</div>
							</div>	
						</g:if>
					
						<g:if test="${conviteInstance?.dataAceite}">
							<div class="col-sm-4">
								<div class="form-group">
									<label for="dataAceite"><i class="glyphicon glyphicon-calendar"></i> <g:message code="convite.dataAceite.label" default="Data do aceite" /></label>
							
									<div><g:formatDate date="${conviteInstance?.dataAceite}" format="dd/MM/yyyy HH:mm:ss" /></div>
								</div>
							</div>	
						</g:if>
					</div>
				</div>
				<div class="panel-footer">		
					<div class="row">
						<div class="col-sm-12">
							<div class="pull-right">
								<g:form action="delete">
									<g:hiddenField name="id" value="${conviteInstance?.id}" />
									<g:link class="btn btn-primary" action="edit" id="${conviteInstance?.id}"><i class="glyphicon glyphicon-pencil"></i> <g:message code="default.button.edit.label" default="Edit" /></g:link>
									<button type="submit" class="btn btn-primary" id="delete" name="delete" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Tem certeza?')}');">
						            	<i class="glyphicon glyphicon-remove"></i> ${message(code: 'default.button.delete.label', default: 'Delete')}
						            </button>
						        </g:form>    
							</div>
						</div>
					</div>
				</div>			
			</div>
		</div>
	</body>
</html>
