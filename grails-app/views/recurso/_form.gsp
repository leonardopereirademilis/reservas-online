<%@ page import="br.com.reservas.Recurso" %>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group ${hasErrors(bean: recursoInstance, field: 'condominio', 'error')} required">
			<label for="condominio">
				<i class="glyphicon glyphicon-home"></i> <g:message code="recurso.condominio.label" default="Nome do condomínio" />
				<span class="required-indicator">*</span>
			</label>
			<g:textField class="form-control" name="condominio.nome" value="${recursoInstance?.condominio?.nome}" disabled=""/>
			<g:hiddenField id="condominio" name="condominio.id" required="" value="${recursoInstance?.condominio?.id}"/>
		</div>
	</div>	
	
	<div class="col-sm-6">
		<div class="form-group ${hasErrors(bean: recursoInstance, field: 'nome', 'error')} ">
			<label for="nome">
				<i class="glyphicon glyphicon-home"></i> <g:message code="recurso.nome.label" default="Nome do Recurso" />
				
			</label>
			<g:textField name="nome" value="${recursoInstance?.nome}" class="form-control"/>
		</div>
	</div>
</div>

<hr>		

<div class="row">
	<div class="col-sm-3">
		<div class="form-group ${hasErrors(bean: recursoInstance, field: 'numeroMaxReservas', 'error')} required">
			<label for="numeroMaxReservas">
				<i class="glyphicon glyphicon-list"></i> <g:message code="recurso.numeroMaxReservas.label" default="Nº máximo de reservas" />
				<span class="required-indicator">*</span>
			</label>
			<g:field class="form-control" name="numeroMaxReservas" type="number" value="${recursoInstance.numeroMaxReservas}" required=""/>
		</div>
	</div>	
	
	<div class="col-sm-3">
		<div class="form-group ${hasErrors(bean: recursoInstance, field: 'tempoReserva', 'error')} required">
			<label for="tempoReserva">
				<i class="glyphicon glyphicon-time"></i> <g:message code="recurso.tempoReserva.label" default="Tempo da reserva" />
				<span class="required-indicator">*</span>
			</label>
			<div class="row">
				<div class="col-sm-6">
					<g:field class="form-control" name="tempoReserva" type="number" value="${recursoInstance.tempoReserva}" required=""/>
				</div>	
				<div class="col-sm-6">
					<g:select id="unidadeTempoReserva" name="unidadeTempoReserva.id" from="${br.com.reservas.UnidadeTempoReserva.list()}" optionKey="id" required="" value="${recursoInstance?.unidadeTempoReserva?.id}" class="many-to-one form-control" noSelection="['':'Unidade de tempo...']"/>
				</div>
			</div>		
		</div>
	</div>
	
	<div class="col-sm-6">
		<div class="col-sm-6">
			<div class="form-group ${hasErrors(bean: recursoInstance, field: 'ativo', 'error')} ">
				<label for="ativo">
					<i class="glyphicon glyphicon-ok-sign"></i> <g:message code="recurso.ativo.label" default="Ativo" />
					
				</label>
				<div>
					<g:checkBox name="ativo" value="${recursoInstance?.ativo}" />
				</div>	
			</div>
		</div>
		
		<div class="col-sm-6">	
			<div class="form-group ${hasErrors(bean: recursoInstance, field: 'exigeConfirmacao', 'error')} ">
				<label for="exigeConfirmacao">
					<i class="glyphicon glyphicon-ok-sign"></i> <g:message code="recurso.exigeConfirmacao.label" default="Exige confirmação" />
					
				</label>
				<div>
					<g:checkBox name="exigeConfirmacao" value="${recursoInstance?.exigeConfirmacao}" />
				</div>	
			</div>
		</div>
	</div>		
</div>		

<div class="row">
	<div class="col-sm-6">
		<div class="form-group ${hasErrors(bean: recursoInstance, field: 'valor', 'error')} ">
			<label for="valor">
				<i class="glyphicon glyphicon-usd"></i> <g:message code="recurso.valor.label" default="Valor (R\$)" />
				
			</label>
			<g:field class="form-control" type="number" step="any" name="valor" value="${fieldValue(bean: recursoInstance, field: 'valor')}"/>
		</div>
	</div>
	
	<div class="col-sm-6">	
		<div class="form-group ${hasErrors(bean: recursoInstance, field: 'capacidade', 'error')} ">
			<label for="capacidade">
				<i class="glyphicon glyphicon-user"></i> <g:message code="recurso.capacidade.label" default="Capacidade (pessoas)" />
				
			</label>
			<g:field class="form-control" name="capacidade" type="number" value="${recursoInstance.capacidade}"/>
		</div>
	</div>
</div>	
		
<div class="row">
	<div class="col-sm-12">		
		<div class="form-group ${hasErrors(bean: recursoInstance, field: 'descricao', 'error')} ">
			<label for="descricao">
				<i class="glyphicon glyphicon-info-sign"></i> <g:message code="recurso.descricao.label" default="Descrição" />
				
			</label>
			<g:textArea class="form-control" name="descricao" value="${recursoInstance?.descricao}"/>
		</div>
	</div>
</div>		

<hr>

<div class="row">
	<div class="col-sm-6">		
		<div class="form-group ${hasErrors(bean: recursoInstance, field: 'tipoReserva', 'error')} ">
			<label for="tipoReserva">
				<i class="glyphicon glyphicon-list"></i> <g:message code="recurso.tipoReserva.label" default="Tipo da reserva" />
				
			</label>
			<g:select name="tipoReserva" from="${br.com.reservas.TipoReserva.list()}" multiple="multiple" optionKey="id" size="5" value="${recursoInstance?.tipoReserva*.id}" class="many-to-many form-control"/>
		</div>
	</div>
	
	<div class="col-sm-6">	
		<div class="form-group ${hasErrors(bean: recursoInstance, field: 'cor', 'error')} required">
			<label for="cor">
				<i class="glyphicon glyphicon-tint"></i> <g:message code="recurso.cor.label" default="Cor" />
				<span class="required-indicator">*</span>
			</label>
			<g:select id="cor" name="cor.id" from="${br.com.reservas.Cor.list()}" optionKey="id" required="" value="${recursoInstance?.cor?.id}" class="many-to-one form-control" noSelection="['':'Selecione uma cor...']" />
		</div>
	</div>
</div>	

<hr>

<div class="row">
	<div class="col-sm-12">
		<div class="form-group ${hasErrors(bean: recursoInstance, field: 'imagens', 'error')} ">
			<label for="imagens">
				<i class="glyphicon glyphicon-picture"></i> <g:message code="recurso.imagens.label" default="Imagens" />
				
			</label>
			
			<ul class="list-group">
				<g:each in="${recursoInstance?.imagens?}" var="i">
				    <li class="list-group-item"><g:link controller="imagem" action="show" id="${i.id}"><img class="imagem-miniatura" src="${createLink(controller:'imagem', action:'imagem', id:i.id)}" /></g:link></li>
				</g:each>
			</ul>
			
			<div class="pull-right">
				<g:if test="${recursoInstance?.id}">
					<g:link class="btn btn-default" controller="imagem" action="create" params="['recurso.id': recursoInstance?.id]"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'imagem.label', default: 'Imagem')])}</g:link>
				</g:if>
				<g:else>
					<div class="btn btn-default disabled"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'imagem.label', default: 'Imagem')])}</div>				
				</g:else>
			</div>		
			
		</div>
	</div>
</div>

<hr>

<div class="row">
	<div class="col-sm-12">		
		<div class="form-group ${hasErrors(bean: recursoInstance, field: 'indisponibilidades', 'error')} ">
			<label for="indisponibilidades">
				<i class="glyphicon glyphicon-ban-circle"></i> <g:message code="recurso.indisponibilidades.label" default="Indisponibilidades" />
				
			</label>
			
			<ul class="list-group">
		<%--		<g:each in="${recursoInstance?.indisponibilidades?}" var="i">--%>
		<%--		    <li class="list-group-item"><g:link controller="indisponibilidade" action="show" id="${i.id}">${i?.encodeAsHTML()}</g:link></li>--%>
		<%--		</g:each>--%>
			</ul>
			
			<div class="pull-right">
				<g:if test="${recursoInstance?.id}">
					<g:link class="btn btn-default" controller="indisponibilidade" action="create" params="['recurso.id': recursoInstance?.id]"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'indisponibilidade.label', default: 'Indisponibilidade')])}</g:link>
				</g:if>
				<g:else>
					<div class="btn btn-default disabled"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'indisponibilidade.label', default: 'Indisponibilidade')])}</div>				
				</g:else>
			</div>		
			
		</div>
	</div>
</div>

<hr>

<div class="row">
	<div class="col-sm-12">		
		<div class="form-group ${hasErrors(bean: recursoInstance, field: 'reserva', 'error')} ">
			<label for="reserva">
				<i class="glyphicon glyphicon-calendar"></i> <g:message code="recurso.reserva.label" default="Reservas" />
				
			</label>
			
			<ul class="list-group">
		<%--		<g:each in="${recursoInstance?.reserva?}" var="r">--%>
		<%--	    	<li class="list-group-item"><g:link controller="reserva" action="show" id="${r.id}">${r?.encodeAsHTML()}</g:link></li>--%>
		<%--		</g:each>--%>
			</ul>
				
			<div class="pull-right">	
				<g:if test="${recursoInstance?.id}">
					<g:link class="btn btn-default" controller="reserva" action="create" params="['recurso.id': recursoInstance?.id]"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'reserva.label', default: 'Reserva')])}</g:link>	
				</g:if>
				<g:else>
					<div class="btn btn-default disabled"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'reserva.label', default: 'Reserva')])}</div>				
				</g:else>	
			</div>	
			
		</div>
	</div>
</div>
	

