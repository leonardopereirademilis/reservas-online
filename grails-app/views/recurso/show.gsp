
<%@ page import="br.com.reservas.Recurso" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'recurso.label', default: 'Recurso')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>		
		<a href="#show-recurso" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<ol class="breadcrumb">
			<li>
				<a class="home" href="${createLink(uri: '/')}">
					<i class="glyphicon glyphicon-home"></i> <g:message code="default.home.label"/>
				</a>
			</li>
			<sec:ifAnyGranted roles="ROLE_ADMIN">
				<li><g:link class="list" action="list"><i class="glyphicon glyphicon-list"></i> <g:message code="default.list.label" args="[entityName]" /></g:link></li>
			</sec:ifAnyGranted>
			<g:if test="${ehAdministrador}">	
				<li><g:link class="create" action="create"><i class="glyphicon glyphicon-plus"></i> <g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</g:if>
		</ol>
		<div id="show-recurso" class="container-fluid" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> ${flash.message}</div>
			</g:if>
			
			<div class="panel panel-default">
				<div class="panel-heading">
					<g:message code="default.show.label" args="[entityName]" />
				</div>
				
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<g:if test="${recursoInstance?.condominio}">
									<label for="condominio"><i class="glyphicon glyphicon-home"></i> <g:message code="recurso.condominio.label" default="Nome do condomínio" /></label>
									<g:link controller="condominio" action="show" id="${recursoInstance?.condominio?.id}"><h2>${recursoInstance?.condominio?.encodeAsHTML()}</h2></g:link>
								</g:if>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">		
								<g:if test="${recursoInstance?.nome}">
									<label for="nome"><i class="glyphicon glyphicon-blackboard"></i> <g:message code="recurso.nome.label" default="Nome do recurso" /></label>
									<h3><g:fieldValue bean="${recursoInstance}" field="nome"/></h3>
								</g:if>
							</div>
						</div>
					</div>
					
					<hr>
					
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">			
<%--								<g:if test="${recursoInstance?.exigeConfirmacao}">--%>
									<label for="exigeConfirmacao"><i class="glyphicon glyphicon-ok-sign"></i> <g:message code="recurso.exigeConfirmacao.label" default="Exige confirmação" /></label>
									<div><g:formatBoolean boolean="${recursoInstance?.exigeConfirmacao}" /></div>
<%--								</g:if>--%>
							</div>
						</div>
						
						<div class="col-sm-4">
							<div class="form-group">
								<g:if test="${recursoInstance?.numeroMaxReservas}">
									<label for="numeroMaxReservas"><i class="glyphicon glyphicon-info-sign"></i> <g:message code="recurso.numeroMaxReservas.label" default="Nº máximo de reservas" /></label>
									<div><g:fieldValue bean="${recursoInstance}" field="numeroMaxReservas"/><g:message code="recurso.reservas.simultaneas.label" default=" reserva(s) simultânea(s)" /></div>
								</g:if>
							</div>
						</div>		
			
						<div class="col-sm-4">
							<div class="form-group">
<%--								<g:if test="${recursoInstance?.ativo}">--%>
									<label for="ativo"><i class="glyphicon glyphicon-ok-sign"></i> <g:message code="recurso.ativo.label" default="Ativo" /></label>
									<div><g:formatBoolean boolean="${recursoInstance?.ativo}" /></div>
<%--								</g:if>--%>
							</div>
						</div>
					</div>
					
					<hr>
					
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">			
								<g:if test="${recursoInstance?.tempoReserva}">
									<label for="tempoReserva"><i class="glyphicon glyphicon-time"></i> <g:message code="recurso.tempoReserva.label" default="Tempo da reserva" /></label>
									<div><g:fieldValue bean="${recursoInstance}" field="tempoReserva"/> ${recursoInstance?.unidadeTempoReserva}</div>
								</g:if>
							</div>
						</div>
						
						<div class="col-sm-4">
							<div class="form-group">		
								<g:if test="${recursoInstance?.valor}">
									<label for="valor"><i class="glyphicon glyphicon-usd"></i> <g:message code="recurso.valor.label" default="Valor" /></label>
									<div><g:formatNumber number="${recursoInstance?.valor}" type="currency" currencyCode="BRL" /></div>
								</g:if>
							</div>
						</div>
						
						<div class="col-sm-4">
							<div class="form-group">		
								<g:if test="${recursoInstance?.capacidade}">
									<label for="capacidade"><i class="glyphicon glyphicon-info-sign"></i> <g:message code="recurso.capacidade.label" default="Capacidade" /></label>
									<div><g:fieldValue bean="${recursoInstance}" field="capacidade"/> pessoas</div>
								</g:if>
							</div>
						</div>
					</div>
					
					<g:if test="${recursoInstance?.tipoReserva || recursoInstance?.cor}">
						<hr>
						<div class="row">
							<g:if test="${recursoInstance?.tipoReserva}">
								<div class="col-sm-4">
									<div class="form-group">
										<label for="tipoReserva"><i class="glyphicon glyphicon-info-sign"></i> <g:message code="recurso.tipoReserva.label" default="Tipo da reserva" /></label>
										<ul class="list-group">
											<g:each in="${recursoInstance.tipoReserva.sort { it.nome }}" var="t">
												<li class="list-group-item">
													${t?.encodeAsHTML()}
												</li>	
											</g:each>
										</ul>
									</div>
								</div>
							</g:if>
							
							<g:if test="${recursoInstance?.cor}">
								<div class="col-sm-4">
									<div class="form-group">
										<label for="cor"><i class="glyphicon glyphicon-tint"></i> <g:message code="recurso.cor.label" default="Cor" /></label>
										<div class="cor${recursoInstance?.cor.id}"><i class="glyphicon glyphicon-tint"></i><i class="glyphicon glyphicon-tint"></i><i class="glyphicon glyphicon-tint"></i></div>
									</div>
								</div>
							</g:if>
						</div>
					</g:if>	
					
					<hr>
					
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">			
								<g:if test="${recursoInstance?.descricao}">
									<label for="descricao"><i class="glyphicon glyphicon-info-sign"></i> <g:message code="recurso.descricao.label" default="Descrição" /></label>
									<div><g:fieldValue bean="${recursoInstance}" field="descricao"/></div>
								</g:if>
							</div>
						</div>
					</div>
				
					<g:if test="${recursoInstance?.imagens}">
						<hr>
						<label for="imagens"><i class="glyphicon glyphicon-picture"></i> <g:message code="recurso.imagens.label" default="Imagens" /></label>
						<div class="row">
							<g:each in="${recursoInstance.imagens}" var="img" status="i">
								<div class="col-sm-3">
									<img class="imagem-miniatura" src="${createLink(controller:'imagem', action:'imagem', id:img.id)}" />
								</div>
							</g:each>	
						</div>	
<%--						<div class="container-fluid">--%>
<%--							<div id="myCarousel" class="carousel slide" data-ride="carousel">--%>
<%--								<!-- Indicators -->--%>
<%--								<ol class="carousel-indicators">--%>
<%--									<g:each in="${recursoInstance.imagens}" var="img" status="i">--%>
<%--										<g:if test="${i == 0}">--%>
<%--											<li data-target="#myCarousel" data-slide-to="${i}" class="active"></li>--%>
<%--										</g:if>--%>
<%--										<g:else>--%>
<%--											<li data-target="#myCarousel" data-slide-to="${i}"></li>--%>
<%--										</g:else>	--%>
<%--									</g:each>--%>
<%--								</ol>--%>
								<!-- Wrapper for slides -->
<%--								<div class="carousel-inner" role="listbox">--%>
<%--									<g:each in="${recursoInstance.imagens}" var="img" status="i">--%>
<%--										<g:if test="${i == 0}">--%>
<%--											<div class="item active">--%>
<%--										</g:if>--%>
<%--										<g:else>--%>
<%--											<div class="item">--%>
<%--										</g:else>--%>
<%--											<img class="imagem-miniatura" src="${createLink(controller:'imagem', action:'imagem', id:img.id)}" />	--%>
<%--											</div>--%>
<%--									</g:each>			--%>
<%--								</div>--%>
				
								<!-- Left and right controls -->
<%--								<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">--%>
<%--									<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>--%>
<%--									<span class="sr-only">Previous</span>--%>
<%--								</a>--%>
<%--								<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">--%>
<%--									<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>--%>
<%--									<span class="sr-only">Next</span>--%>
<%--								</a>--%>
<%--							</div>--%>
<%--						</div>	--%>
					</g:if>
				
					<g:if test="${ehAdministrador}">
						<hr>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label for="indisponibilidades"><i class="glyphicon glyphicon-ban-circle"></i> <g:message code="recurso.indisponibilidades.label" default="Indisponibilidades" /></label>
						
									<ul class="list-group">
									</ul>
						
									<div class="pull-right">
										<g:if test="${recursoInstance?.id}">
											<g:link class="btn btn-default" controller="indisponibilidade" action="create" params="['recurso.id': recursoInstance?.id]"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'indisponibilidade.label', default: 'Indisponibilidade')])}</g:link>
										</g:if>
										<g:else>
											<div class="btn btn-default disabled"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'indisponibilidade.label', default: 'Indisponibilidade')])}</div>				
										</g:else>	
									</div>
								</div>
							</div>
						</div>								
					</g:if>
					
					<hr>
					
					<g:render template="/recurso/calendar"/>
					
				</div>
							
				<g:if test="${ehAdministrador}">
					<div class="panel-footer">
						<div class="row">
							<div class="col-sm-12">
								<div class="pull-right">
									<g:form action="delete">
										<fieldset class="buttons">
											<g:hiddenField name="id" value="${recursoInstance?.id}" />
											<g:link class="btn btn-primary" action="edit" id="${recursoInstance?.id}"><i class="glyphicon glyphicon-pencil"></i> <g:message code="default.button.edit.label" default="Edit" /></g:link>
											<button type="submit" class="btn btn-primary" id="delete" name="delete" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Tem certeza?')}');">
							                	<i class="glyphicon glyphicon-remove"></i> ${message(code: 'default.button.delete.label', default: 'Delete')}
							                </button>
										</fieldset>
									</g:form>
								</div>
							</div>
						</div>
					</div>
				</g:if>
			</div>
		</div>	
	</body>
</html>
