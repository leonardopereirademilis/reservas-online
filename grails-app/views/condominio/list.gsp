
<%@ page import="br.com.reservas.Condominio" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'condominio.label', default: 'Condominio')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-condominio" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
				
		<ol class="breadcrumb" >
			<li>
				<a href="${createLink(uri: '/')}">
					<i class="glyphicon glyphicon-home"></i> <%-- <g:message code="default.home.label"/> --%>
				</a>
			</li>
						
			<g:if test="${verificarCriacaoCondominio}">
				<li>
					<g:link class="create" action="create">
						<i class="glyphicon glyphicon-plus"></i> <g:message code="default.new.label" args="[entityName]" />
					</g:link>
				</li>
			</g:if>
			<sec:ifAnyGranted roles="ROLE_ADMIN">
				<g:if test="${!params.todos}">
					<li>
						<g:link class="list" action="list" params="['todos':true]">
							<i class="glyphicon glyphicon-list"></i> <g:message code="default.todos.condominio.label" args="[entityName]" default="Todos os Condomínios"/>
						</g:link>
					</li>
				</g:if>	
			</sec:ifAnyGranted>
			<g:if test="${params.todos}">
				<li>
					<g:link class="list" action="list">
						<i class="glyphicon glyphicon-list"></i> <g:message code="default.meus.condominio.label" args="[entityName]" default="Meus Condomínios"/>
					</g:link>
				</li>
			</g:if>
			<li>
				<g:link class="list" controller="reserva" action="list">
					<i class="glyphicon glyphicon-list"></i> <g:message code="default.list.reservas.label" default="Lista de Reservas" />
				</g:link>
			</li>
		</ol>
		
		<div id="meusCondominioseConvites">
			<g:render template="meusCondominioseConvites"></g:render>
		</div>
				
		<div id="minhasReservas" class="container-fluid">
			${reserva.buscarReservasList([params: params])}
		</div>
		
		<div id="minhasReservas" class="container-fluid bg-grey">
			<g:render template="search"></g:render>
		</div>	
			
	</body>
</html>
