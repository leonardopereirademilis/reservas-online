<%@ page import="br.com.reservas.Condominio" %>

<div class="row">
	<div class="col-sm-4">
		<div class="form-group ${hasErrors(bean: condominioInstance, field: 'nome', 'error')} required">
			<label for="nome">
				<i class="glyphicon glyphicon-home"></i> <g:message code="condominio.nome.label" default="Nome do Condomínio" />
				<span class="required-indicator">*</span>
			</label>
			<g:textField class="form-control" name="nome" value="${condominioInstance?.nome}" required=""/>
		</div>
	</div>
</div>		

<hr>

<div class="row">
	<div class="col-sm-12">
		<g:render template="/endereco/form" model="['enderecoInstance':condominioInstance?.endereco]"/>
	</div>
</div>		

<g:if test="${condominioInstance?.id}">
	<hr>
	<div class="row">
		<div class="col-sm-4">
			<div class="form-group ${hasErrors(bean: condominioInstance, field: 'administradores', 'error')} ">
				<label for="administradores">
					<i class="glyphicon glyphicon-user"></i> <g:message code="condominio.administradores.label" default="Administradores" />
					
				</label>
				<g:select name="administradores" from="${br.com.reservas.Usuario.list()}" multiple="multiple" optionKey="id" size="5" value="${condominioInstance?.administradores*.id}" class="many-to-many form-control"/>
			</div>
		</div>
	</div>		
</g:if>
<g:else>
	<g:hiddenField name="administradores" value="${usuarioInstance?.id}" />
</g:else>
			

<hr>

<div class="row">
	<div class="col-sm-12">
		<div class="form-group ${hasErrors(bean: condominioInstance, field: 'apartamentos', 'error')} ">
			<label for="apartamentos">
				<i class="glyphicon glyphicon-list"></i> <g:message code="condominio.apartamentos.label" default="Apartamentos" />
				
			</label>
			
			<ul class="list-group">
							
				<g:if test="${condominioInstance?.apartamentos?.size() > 5}">
					<li class="list-group-item">
						<div class="fieldcontain-list-item">
							<span><g:link controller="apartamento" action="list" params="['condominio':condominioInstance.id]">${condominioInstance.apartamentos.size()}<g:message code="condominio.apartamentos.label" default=" apartamentos" /></g:link></span>
						</div>
					</li>	
				</g:if>
				<g:else>
					<g:each in="${condominioInstance?.apartamentos?.sort { it.id }}" var="a">
					    <li class="list-group-item">
					    	<div class="fieldcontain-list-item">
					    		<span>
					    			<g:link controller="apartamento" action="show" id="${a.id}">${a?.toHTML()}</g:link>
					    		</span>
					    	</div>		
					    </li>
					</g:each>
				</g:else>
			</ul>			
			
			<div class="pull-right">
				<g:if test="${condominioInstance?.id}">
					<g:link class="btn btn-default " controller="apartamento" action="create" params="['condominio.id': condominioInstance?.id]"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'apartamento.label', default: 'Apartamento')])}</g:link>	
				</g:if>
				<g:else>
					<div class="btn btn-default disabled"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'apartamento.label', default: 'Apartamento')])}</div>				
				</g:else>
			</div>				
			
		</div>
	</div>
</div>		

<hr>

<div class="row">
	<div class="col-sm-12">
		<div class="form-group ${hasErrors(bean: condominioInstance, field: 'recursos', 'error')} ">
			<label for="recursos">
				<i class="glyphicon glyphicon-list"></i> <g:message code="condominio.recursos.label" default="Recursos" />
				
			</label>
			
			<ul class="list-group">
				<g:each in="${condominioInstance?.recursos?}" var="r">
				    <li class="list-group-item">
				    	<div class="fieldcontain-list-item">
				    		<span>
				    			<g:link controller="recurso" action="show" id="${r.id}">${r?.encodeAsHTML()}</g:link>
				    		</span>	
				    	</div>	
				    </li>
				</g:each>
			</ul>	
			
			<div class="pull-right">
				<g:if test="${condominioInstance?.id}">
					<g:link class="btn btn-default" controller="recurso" action="create" params="['condominio.id': condominioInstance?.id]"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'recurso.label', default: 'Recurso')])}</g:link>	
				</g:if>
				<g:else>
					<div class="btn btn-default disabled"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'recurso.label', default: 'Recurso')])}</div>				
				</g:else>			
			</div>
			
		</div>
	</div>
</div>

<hr>

<div class="row">
	<div class="col-sm-12">		
		<div class="form-group ${hasErrors(bean: condominioInstance, field: 'mensalidades', 'error')} ">
			<label for="mensalidades">
				<i class="glyphicon glyphicon-list"></i> <g:message code="condominio.mensalidades.label" default="Mensalidades" />
				
			</label>
			
			<ul class="list-group">
				<g:each in="${condominioInstance?.mensalidades?}" var="m">
				    <li class="list-group-item">
				    	<div class="fieldcontain-list-item">
				    		<span>
				    			<g:link controller="mensalidade" action="show" id="${m.id}">${m?.encodeAsHTML()}</g:link>
				    		</span>
				    	</div>		
				    			
				    </li>
				    			
				</g:each>
			</ul>
			
			<div class="pull-right">
				<g:if test="${condominioInstance?.id}">
					<g:link class="btn btn-default" controller="mensalidade" action="create" params="['condominio.id': condominioInstance?.id]"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'mensalidade.label', default: 'Mensalidade')])}</g:link>	
				</g:if>
				<g:else>
					<div class="btn btn-default disabled"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'mensalidade.label', default: 'Mensalidade')])}</div>				
				</g:else>
			</div>
		
		</div>
	</div>
</div>	