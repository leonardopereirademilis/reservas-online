<%@ page import="br.com.reservas.Condominio" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'condominio.label', default: 'Condominio')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#create-condominio" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<ol class="breadcrumb">
			<li><a class="home" href="${createLink(uri: '/')}"><i class="glyphicon glyphicon-home"></i> <g:message code="default.home.label"/></a></li>
			<li><g:link class="list" action="list"><i class="glyphicon glyphicon-list"></i> <g:message code="default.list.label" args="[entityName]" /></g:link></li>
		</ol>
		<div id="create-condominio" class="container-fluid" role="main">
			<h1><g:message code="default.create.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> ${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${condominioInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${condominioInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form action="save" >
				<div class="panel panel-default">
				    <div class="panel-heading"><g:message code="default.create.label" args="[entityName]" /></div>
				    
				    <div class="panel-body">
				    
						<g:render template="form"/>
						
					</div>	
					<div class="panel-footer">
						<div class="row">
							<div class="col-sm-12">
								<div class="pull-right">
									<button type="submit" class="btn btn-primary" id="create" name="create">
						               	<i class="glyphicon glyphicon-floppy-disk"></i> ${message(code: 'default.button.create.label', default: 'Create')}
						            </button>
								</div>
							</div>
						</div>
					</div>	
				</div>
			</g:form>
		</div>
	</body>
</html>
