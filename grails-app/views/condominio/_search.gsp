			<h1><g:message code="condominio.buscar.label" default="Buscar Condomínios" /></h1>
			
			<g:formRemote name="search" url="[action:'search']" update="searchResult">	
				<div class="panel panel-default">
					<div class="panel-heading"><g:message code="condominio.buscar.label" default="Buscar Condomínios" /></div>
				    
				    <div class="panel-body">
						<div class="row">
							<div class="form-group col-sm-4">
								<label for="nome">
									<g:message code="condominio.nome.label" default="Nome" />
									<span class="required-indicator">*</span>
								</label>
								<g:textField class="form-control" name="nome" value="${params.nome}" required="true"/>
							</div>	
						
							<div class="form-group col-sm-4">
								<label for="estado">
									<g:message code="condominio.estado.label" default="Estado" />
									<span class="required-indicator">*</span>
								</label>
								<g:select id="estado" name="estado.id" from="${br.com.reservas.Estado.list()}" optionKey="id" required="" value="" class="form-control many-to-one" noSelection="['':'Selecione um Estado...']" onchange="${remoteFunction(controller: 'cidade', action: 'buscaCidades', params: '\'estado=\' + this.value', update:'cidadeSelect')}" />
							</div>
							
							<div class="form-group col-sm-4">	
								<label for="cidade">
									<g:message code="condominio.cidade.label" default="Cidade" />
									<span class="required-indicator">*</span>
								</label>
								<div id="cidadeSelect" style="display: inline;">
									<g:render template="/cidade/cidade" model="[cidades: cidades]" ></g:render>
								</div>
							</div>	
						</div>
					</div>
					<div class="panel-footer">		
						<div class="row">
							<div class="col-sm-12">
								<div class="pull-right">				
							    	<button type="submit" class="btn btn-primary" id="search" name="search">
					                	<i class="glyphicon glyphicon-search"></i> Buscar
					                </button>
							    </div>
							</div>    
					    </div>
					</div>
				</div>	        
	    	</g:formRemote>
			
			<div class="row">
				<div class="form-group col-sm-12">
					<div id="searchResult"></div>
				</div>
			</div>	