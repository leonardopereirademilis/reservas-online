	<h1><g:message code="default.show.label" args="['Condomínio']" /></h1>
	<div class="row">
		<g:if test="${condominioInstance?.nome}">
			<div class="col-sm-6">
				<div class="form-group">
					<label for="nome"><i class="glyphicon glyphicon-home"></i> <g:message code="condominio.nome.label" default="Nome do Condomínio" /></label>
					<h2><g:link controller="condominio" action="show" id="${condominioInstance?.id}"><g:fieldValue bean="${condominioInstance}" field="nome"/></g:link></h2>
				</div>
			</div>		
		</g:if>
			
		<g:if test="${condominioInstance?.endereco}">
			<div class="col-sm-6">
				<div class="form-group">
					<label for="endereco"><i class="glyphicon glyphicon-map-marker"></i> <g:message code="condominio.endereco.label" default="Endereço" /></label>
					<div>${condominioInstance?.endereco?.toHTML()}</div>
				</div>
			</div>
		</g:if>
	</div>	
	
	<hr>
	
	<div class="row">		
		<g:if test="${condominioInstance?.administradores}">
			<div class="col-sm-4">
				<div class="form-group">
					<label for="administradores"><i class="glyphicon glyphicon-user"></i> <g:message code="condominio.administradores.label" default="Administradores" /></label>
					<ul class="list-group">
						<g:each in="${condominioInstance.administradores}" var="a">
							<li class="list-group-item">
								<g:link controller="usuario" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link>
							</li>	
						</g:each>
					</ul>
				</div>			
			</div>
		</g:if>
			
		<g:if test="${condominioInstance?.apartamentos}">
			<div class="col-sm-4">
				<div class="form-group">
					<label for="apartamentos"><i class="glyphicon glyphicon-list"></i> <g:message code="condominio.apartamentos.label" default="Apartamentos" /></label>
					<ul class="list-group">
						<g:each in="${condominioInstance.apartamentos}" var="a">
							<li class="list-group-item">
								<g:link controller="apartamento" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link>
							</li>	
						</g:each>
					</ul>	
				</div>							
			</div>
		</g:if>
			
		<g:if test="${condominioInstance?.recursos}">
			<div class="col-sm-4">
				<div class="form-group">
					<label for="recursos"><i class="glyphicon glyphicon-list"></i> <g:message code="condominio.recursos.label" default="Recursos" /></label>
					<ul class="list-group">
						<g:each in="${condominioInstance.recursos}" var="r">
							<li class="list-group-item">
								<g:link controller="recurso" action="show" id="${r.id}">${r?.encodeAsHTML()}</g:link>
							</li>	
						</g:each>
					</ul>	
				</div>	
			</div>
		</g:if>
	</div>	