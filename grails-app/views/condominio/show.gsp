
<%@ page import="br.com.reservas.Condominio" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'condominio.label', default: 'Condominio')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-condominio" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<ol class="breadcrumb">
			<li>
				<a href="${createLink(uri: '/')}">
					<i class="glyphicon glyphicon-home"></i> <%-- <g:message code="default.home.label"/> --%>
				</a>
			</li>
			<li><g:link class="list" action="list"><i class="glyphicon glyphicon-list"></i> <g:message code="default.list.label" args="[entityName]" /></g:link></li>
			<g:if test="${verificarCriacaoCondominio}">
				<li><g:link class="create" action="create"><i class="glyphicon glyphicon-plus"></i> <g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</g:if>	
		</ol>
		
		<div id="show-condominio" class="container-fluid" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> ${flash.message}</div>
			</g:if>
			
			<div class="panel panel-default">
				<div class="panel-heading">
					<g:message code="default.show.label" args="[entityName]" />
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<g:if test="${condominioInstance?.nome}">
									<label for="nome">
										<i class="glyphicon glyphicon-home"></i> <g:message code="condominio.nome.label" default="Nome do Condomínio" />
									</label>	
									
									<h2><g:fieldValue bean="${condominioInstance}" field="nome"/></h2>									
								</g:if>
							</div>
						</div>		
						
						<div class="col-sm-6">
							<div class="form-group">
								<g:if test="${condominioInstance?.endereco}">
									<label for="endereco"><i class="glyphicon glyphicon-map-marker"></i> <g:message code="condominio.endereco.label" default="Endereço" /></label>
								
									<div>${condominioInstance?.endereco?.toHTML()}</div>
								</g:if>
							</div>	
						</div>
					</div>
					
					<hr>		
					
					<g:if test="${condominioInstance?.apartamentos || ehAdministrador}">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<g:if test="${condominioInstance?.apartamentos}">
										<label for="apartamentos"><i class="glyphicon glyphicon-list"></i> <g:message code="condominio.apartamentos.label" default="Apartamentos" /></label>
											
										<g:if test="${condominioInstance.apartamentos.size() > 5}">
											<ul class="list-group">
												<g:if test="${ehAdministrador}">
													<li class="list-group-item">
														<g:link controller="apartamento" action="list" params="['condominio':condominioInstance.id]">
															${condominioInstance.apartamentos.size()}<g:message code="condominio.apartamentos.label" default=" apartamentos" />
														</g:link>
													</li>	
												</g:if>
												<g:else>
														<li class="list-group-item">${condominioInstance.apartamentos.size()}<g:message code="condominio.apartamentos.label" default=" apartamentos" /></li>
												</g:else>
											</ul>	
										</g:if>
										<g:else>
											<ul class="list-group">
												<g:each in="${condominioInstance.apartamentos.sort { it.id }}" var="a">
													<g:if test="${ehAdministrador}">
														<li class="list-group-item">
															<g:link controller="apartamento" action="show" id="${a.id}">
																${a?.toHTML()}
															</g:link>
														</li>	
													</g:if>
													<g:else>
														<li class="list-group-item">${a?.toHTML()}</li>
													</g:else>
												</g:each>
											</ul>
										</g:else>
									</g:if>
									
									<g:if test="${ehAdministrador}">
										<div class="pull-right">
											<g:if test="${condominioInstance?.id}">
												<g:link class="btn btn-default" controller="apartamento" action="create" params="['condominio.id': condominioInstance?.id]"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'apartamento.label', default: 'Apartamento')])}</g:link>	
											</g:if>
											<g:else>
												<div class="btn btn-default disabled"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'apartamento.label', default: 'Apartamento')])}</div>				
											</g:else>
										</div>				
									</g:if>	
								</div>
							</div>							
						</div>
					</g:if>					
					
					<hr>
					
					<div class="row">
						<div class="form-group col-sm-12">	
							<g:if test="${condominioInstance?.administradores}">
								<label for="administradores"><i class="glyphicon glyphicon-user"></i> <g:message code="condominio.administradores.label" default="Administradores" /></label>
								
								<ul class="list-group">
									<g:each in="${condominioInstance.administradores.sort { it.nome }}" var="a">
										<li class="list-group-item">
											<g:link controller="usuario" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link>
										</li>	
									</g:each>
								</ul>	
							</g:if>
						</div>
					</div>
					
					<hr>					
							
					<g:if test="${condominioInstance?.recursos || ehAdministrador}">
						<g:if test="${condominioInstance?.recursos}">
							<label for="recursos"><i class="glyphicon glyphicon-list"></i> <g:message code="condominio.recursos.label" default="Recursos" /></label>
												
							<ul class="list-group">
								<g:if test="${!condominioInstance.verificarUsuario() && !condominioInstance.verificarAdministrador()}">
									<g:each in="${condominioInstance.recursos.sort { it.nome }}" var="r">
										<li class="list-group-item">${r?.encodeAsHTML()}</li>
									</g:each>
								</g:if>
								<g:else>
									<g:each in="${condominioInstance.recursos.sort { it.nome }}" var="r">
										<li class="list-group-item"><g:link controller="recurso" action="show" id="${r.id}">${r?.encodeAsHTML()}</g:link></li>	
									</g:each>
								</g:else>
							</ul>		
						</g:if>
							
						<g:if test="${ehAdministrador}">
							<div class="pull-right">
								<g:if test="${condominioInstance?.id}">
									<g:link class="btn btn-default" controller="recurso" action="create" params="['condominio.id': condominioInstance?.id]"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'recurso.label', default: 'Recurso')])}</g:link>	
								</g:if>
								<g:else>
									<div class="btn btn-default disabled"><i class="glyphicon glyphicon-plus"></i> ${message(code: 'default.add.label', args: [message(code: 'recurso.label', default: 'Recurso')])}</div>				
								</g:else>			
							</div>	
						</g:if>
					</g:if>				
				</div>		
					
				<g:if test="${condominioInstance.verificarAdministrador()}">
					<div class="panel-footer">
						<div class="row">
							<div class="col-sm-12">
								<div class="pull-right">
									<g:form action="delete">
										<fieldset class="buttons">
											<g:hiddenField name="id" value="${condominioInstance?.id}" />
											<g:link class="btn btn-primary" action="edit" id="${condominioInstance?.id}"><i class="glyphicon glyphicon-pencil"></i> <g:message code="default.button.edit.label" default="Edit" /></g:link>
											<button type="submit" class="btn btn-primary" id="delete" name="delete" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Tem certeza?')}');">
							                	<i class="glyphicon glyphicon-remove"></i> ${message(code: 'default.button.delete.label', default: 'Delete')}
							                </button>
										</fieldset>
									</g:form>
								</div>
							</div>
						</div>
					</div>				
				</g:if>
			</div>
		</div>	
	
		<g:if test="${!condominioInstance.verificarUsuario()}">
			<div id="permissao" class="container-fluid bg-grey">
				<g:render template="permissao"></g:render>
			</div>						
		</g:if>	
	
	</body>	
</html>
