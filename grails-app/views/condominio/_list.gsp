	<g:if test="${condominioInstanceTotal}">
		<div class="panel panel-default">
			<div class="panel-heading"><g:message code="default.list.encontrados.label" default="Condomínios Encontrados" /></div>
					    
			<div class="panel-body">
				<div id="list-condominio" class="content scaffold-list" role="main">
					<g:if test="${flash.message}">
						<div class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> 
							${flash.message}
						</div>
					</g:if>
						
					<div class="table-responsive">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									
									<th><i class="glyphicon glyphicon-home"></i> <g:message code="condominio.nome.label" default="Nome do Condomínio" /></th>
										
									<th><i class="glyphicon glyphicon-map-marker"></i> <g:message code="condominio.endereco.label" default="Endereço" /></th>
									
								</tr>
							</thead>
							<tbody>
							<g:each in="${condominioInstanceList}" status="i" var="condominioInstance">
								<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
									
									<td><g:link action="show" id="${condominioInstance.id}">${fieldValue(bean: condominioInstance, field: "nome")}</g:link></td>
									
									<td>${condominioInstance.endereco.toHTML()}</td>
									
								</tr>
							</g:each>
							</tbody>
						</table>
					</div>	
					
					<div class="row text-center">
						<div class="pagination">
							<g:paginate total="${condominioInstanceTotal}" />
						</div>
					</div>	
				</div>
			</div>
		</div>		
	</g:if>
	<g:else>
		<div class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> 
			<g:message code="condominio.nenhum.resultado.label" default="Nenhum resultado foi encontrado" />
		</div>
	</g:else>