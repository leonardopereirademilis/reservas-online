			<h1><g:message code="default.list.condominio.label" default="Meus Condomínios" /></h1>
			<g:if test="${flash.message}">
				<div class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> 
					${flash.message}
				</div>
			</g:if>
			
			<div class="panel panel-default">
				<div class="panel-heading">
					<g:message code="default.list.condominio.label" default="Meus Condomínios" />
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-12">
							<g:if test="${condominioInstanceList.size() > 0}">
								<div class="table-responsive">
									<table class="table table-striped table-bordered">
										<thead>
											<tr>
											
												<th><i class="glyphicon glyphicon-home"></i> <g:message code="condominio.nome.label" default="Nome do Condomínio" /></th>
											
												<th><i class="glyphicon glyphicon-map-marker"></i> <g:message code="condominio.endereco.label" default="Endereço" /></th>
												
												<th><i class="glyphicon glyphicon-list"></i> <g:message code="condominio.recurso.label" default="Recursos" /></th>
												
												<th><i class="glyphicon glyphicon-user"></i> <g:message code="condominio.administradores.label" default="Administradores" /></th>
											
											</tr>
										</thead>
										<tbody>
										<g:each in="${condominioInstanceList}" status="i" var="condominioInstance">
											<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
											
												<td><h3><g:link action="show" id="${condominioInstance.id}">${fieldValue(bean: condominioInstance, field: "nome")}</g:link></h3></td>
											
												<td>${condominioInstance.endereco.toHTML()}</td>
												
												<td>
													<ul class="list-group">
													<g:each in="${condominioInstance.recursos.sort { it.nome }}" status="k" var="recursoInstance">
														<li class="list-group-item">
															<g:link controller="recurso" action="show" id="${recursoInstance.id}">${recursoInstance}</g:link>
														</li>	
													</g:each>
													</ul>
												</td>
												
												<td>
													<ul class="list-group">
													<g:each in="${condominioInstance.administradores}" status="k" var="administradorInstance">
														<li class="list-group-item">
															<g:link controller="usuario" action="show" id="${administradorInstance.id}">${fieldValue(bean: administradorInstance, field: "nome")}</g:link>
														</li>	
													</g:each>
													</ul> 
												</td>
											
											</tr>
										</g:each>
										</tbody>
									</table>
								</div>
								<div class="row text-center">
									<div class="pagination">
										<g:paginate total="${condominioInstanceTotal}" />
									</div>
								</div>
							</g:if>
							<g:else>
								<div class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> 
									<g:message code="condominio.nenhum.resultado.label" default="Nenhum resultado foi encontrado" />
								</div>
							</g:else>
						</div>
					</div>
				</div>
			</div>			