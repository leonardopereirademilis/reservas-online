<h1><g:message code="default.convites.pendentes.label" default="Convites pendentes" args="[entityName]" /></h1>
<div class="panel panel-default">
   	<div class="panel-heading"><g:message code="default.convites.pendentes.label" default="Convites pendentes" args="[entityName]" /></div>
   	<div class="panel-body">
		<g:if test="${condominioInstance.verificarAdministrador()}">
			
			<g:if test="${convitesPendentesList.size() > 0}">
				<table class="table table-bordered table-striped">
					<thead>
						<th><g:message code="convite.usuario.label" default="Usuário" /></th>
						<th><g:message code="convite.email.label" default="E-mail" /></th>
						<th><g:message code="convite.apartamento.label" default="Apartamento" /></th>
						<th><g:message code="convite.data.solicitacao.label" default="Data da Solicitação" /></th>
						<th><g:message code="convite.acao.label" default="Ação" /></th>
					</thead>
					<tbody>
						<g:each in="${convitesPendentesList}" var="c">
							<tr>
								<td>
									<g:link controller="usuario" action="show" id="${c.usuario.id}">
										<g:fieldValue bean="${c}" field="usuario"/>
									</g:link>
								</td>
								<td>
									<g:fieldValue bean="${c}" field="email"/>
								</td>
								<td>
									<g:fieldValue bean="${c}" field="apartamento"/>
								</td>								
								<td>	
									<g:formatDate date="${c.dataConvite}" format="dd/MM/yyyy HH:mm:ss"/>
								</td>
								<td>
									<g:if test="${c.usuarioSolicitou}">
										<div class="form-inline">
											<div class="form-group">
												<g:formRemote name="permissao_form" url="[controller:'convite', action:'atualizarConvite']" update="permissao">
													<g:hiddenField name="id" value="${c.id}"/>
													<g:hiddenField name="condominioInstanceId" value="${condominioInstance.id}"/>
													<g:hiddenField name="aprovado" value="1"/>	
													<g:hiddenField name="template" value="permissao"/>						
													<button type="submit" class="btn btn-success" id="aceitarConvite" name="aceitarConvite">
												    	<i class="glyphicon glyphicon-ok"></i> Sim
												    </button>							
												</g:formRemote>
											</div>	
											
											<div class="form-group">
												<g:formRemote name="permissao_form" url="[controller:'convite', action:'atualizarConvite']" update="permissao">
													<g:hiddenField name="id" value="${c.id}"/>
													<g:hiddenField name="condominioInstanceId" value="${condominioInstance.id}"/>
													<g:hiddenField name="aprovado" value="0"/>			
													<g:hiddenField name="template" value="permissao"/>									
													<button type="submit" class="btn btn-danger" id="negarConvite" name="negarConvite">
												    	<i class="glyphicon glyphicon-remove"></i> Não
												    </button>
												</g:formRemote>
											</div>											
										</div>
									</g:if>
									<g:else>
										<g:message code="condominio.aguardando.label" default="Aguardando aceite do Usuário" />
									</g:else>	
								</td>					
							</tr>			
						</g:each>
					</tbody>	
				</table>
			</g:if>
			<g:else>
				<div class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> Não existem convites pendentes</div>
			</g:else>	
		</g:if>
		<g:else>	
			<g:if test="${condominioInstance.verificarConvite()}">
				<div class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> <g:message code="condominio.aprovacao.label" default="Sua solicitação já foi feita e aguarda aprovação." /></div>
			</g:if>
			<g:else>
				<div class="alert alert-info"><i class="glyphicon glyphicon-alert"></i> <g:message code="condominio.nao.usuario.label" default="Você não possui permissão para este condomínio." /></div>
					
				<hr>
										
				<g:formRemote name="permissao_form" url="[controller:'convite', action:'criarConvite']" update="permissao">
					<g:hiddenField name="condominioInstanceId" value="${condominioInstance.id}"/>
					
					<div class="form-group ${hasErrors(bean: enderecoInstance, field: 'apartamento', 'error')} required">
						<label for="apartamento">
							<i class="glyphicon glyphicon-list"></i> 
							<g:message code="condominio.apartamento.label" default="Apartamento" />
							<span class="required-indicator">*</span>
						</label>
						<g:select id="apartamento" name="apartamento.id" from="${condominioInstance?.apartamentos.sort { it.numero }}" optionKey="id" required="" value="" class="many-to-one form-control" noSelection="['':'Selecione um Apartamento...']" />
					</div>
											
					<div class="pull-right">											 
						<button type="submit" class="btn btn-default" id="permissao" name="permissao">
					    	<i class="glyphicon glyphicon-plus"></i> Solicitar permissão
					    </button>
					</div>	
					
											
				</g:formRemote>
			</g:else>
		</g:else>
	</div>
</div>		