package br.com.reservas

class ConviteTagLib {

	def conviteService
	def springSecurityService
	
	static namespace = "convite"
			  
	def buscarConvitesPendentesList = { attrs ->
		def usuario = springSecurityService.currentUser
		
		out << render(template:"/convite/list", model:['convitesPendentesList':conviteService.buscarConvitesPendentesList(attrs.params), usuario:usuario])
	}	
	
}
