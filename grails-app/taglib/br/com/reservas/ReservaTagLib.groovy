package br.com.reservas

class ReservaTagLib {
	def reservaService
	def springSecurityService
	
	static namespace = "reserva"
			  
	def buscarReservasList = { attrs ->
		def usuario = springSecurityService.currentUser
		
		def reservaInstanceList = reservaService.buscarReservasList(attrs.params)
		
		out << render(template:"/reserva/list", model:['reservaInstanceList': reservaInstanceList, 'reservaInstanceTotal':reservaInstanceList.totalCount, usuario:usuario])
	}
}
