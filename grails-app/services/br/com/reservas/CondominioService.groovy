package br.com.reservas

import org.hibernate.Criteria;

class CondominioService {

    def springSecurityService
	def condominioInstanceTotal
    	
	def buscarCondominiosList(params) {
		Usuario usuario = springSecurityService.currentUser
		
		def condominioCriteria = Condominio.createCriteria()
		def condominioInstanceListIds = condominioCriteria.list(max: params.max?:10, offset: params.offset?:0){
			createAlias('usuarios', 'usuarios', Criteria.LEFT_JOIN)
			createAlias("administradores", "administradores", Criteria.LEFT_JOIN)
						
			or {
				eq('administradores.id', usuario.id)
				eq('usuarios.id', usuario.id)
			}
			
			distinct("id")
		}
				
		List<Condominio> condominioInstanceList = new ArrayList<Condominio>()
		
		for (condominioInstanceListId in condominioInstanceListIds) {
			condominioInstanceList.add(Condominio.get(condominioInstanceListId))
		}
		
		condominioInstanceTotal = condominioInstanceListIds.totalCount
						
		return condominioInstanceList
	}	
}
