package br.com.reservas

class ConviteService {
	
	def springSecurityService
    	
	def buscarConvitesPendentesList(params) {
		Usuario usuario = springSecurityService.currentUser
		
		def conviteCriteria = Convite.createCriteria()
		def convitesPendentesList = conviteCriteria.list(max: params.max?:10, offset: params.offset?:0){
			or{
				and{
					eq('usuario.id', usuario.id)
					isNull('dataAceite')
				}
					
				and{
					apartamento {
						condominio {
							administradores{
								eq('id', usuario.id)
							}						
						}						
					}
					isNull('dataAceite')
				}					
			}
				
			order("dataConvite", "asc")	
		}
		
		return convitesPendentesList
	}		
}
